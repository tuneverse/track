package app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	serviceconfig "track/config"
	"track/internal/cloud/awsutils"
	"track/internal/consts"
	"track/internal/controllers"
	"track/internal/entities"
	"track/internal/middlewares"
	"track/internal/repo"
	"track/internal/repo/driver"
	"track/internal/usecases"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	"gitlab.com/tuneverse/toolkit/core/awsmanager"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
	"gitlab.com/tuneverse/toolkit/utils"
)

var log *logger.Logger

// Run initializes environment configuration, logging, database connection, and API routing.
// It sets up the necessary components and routes for the application and launches it.
func Run() {
	// init the env config
	cfg, err := serviceconfig.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	//creating a new logger.

	// Create a file logger configuration with specified settings.
	file := &logger.FileMode{
		LogfileName:  "track.log",
		LogPath:      "logs",
		LogMaxAge:    7,
		LogMaxSize:   1024 * 1024 * 10,
		LogMaxBackup: 5,
	}
	// Configuring client options for the logger.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
		JSONFormater:        false,
	}
	if cfg.Debug {
		log = logger.InitLogger(clientOpt, file)
	} else {
		// Create a database logger configuration with the specified URL and secret.
		db := &logger.CloudMode{
			URL:    cfg.LoggerServiceURL,
			Secret: cfg.LoggerSecret,
		}
		// Initialize the logger with the specified configurations for database, file, and console logging.
		log = logger.InitLogger(clientOpt, db, file)
	}

	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database, err=%s", err.Error())
		return
	}

	//initialises aws
	awsConfig, err := awsInit(cfg)
	if err != nil {
		log.Fatalf("Failed to initialize aws, err=%s", err.Error())
		return
	}

	queue, err := driver.InitQueue(cfg, awsConfig)
	if err != nil {
		log.Fatalf("unable to connect the rabbitMQ, err=%s", err.Error())
		return
	}
	log.Info("queue initialised successfully")

	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}
	// middleware initialization
	// m := middlewares.NewMiddlewares(cfg)
	m := middlewares.NewMiddlewares(cfg)
	api := router.Group("/api")
	api.Use(middleware.LogMiddleware(map[string]interface{}{}))
	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))
	api.Use(m.JwtDecoding())
	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          consts.CacheErrorKey,
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", cfg.LocalisationServiceURL),
		},
	))
	api.Use(middleware.EndpointExtraction(
		middleware.EndPointOptions{
			Cache:            cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:  time.Duration(time.Hour * 24),
			CacheKeyLabel:    consts.CacheEndpointsKey,
			ContextEndPoints: consts.ContextEndPoints,
			EndPointsURL:     fmt.Sprintf("%s/localization/endpointname", cfg.EndpointURL),
		},
	))
	api.Use(m.JwtDecoding())

	// complete user related initialization
	{

		cloud := awsutils.NewCloudService(awsConfig)
		// repo initialization
		trackRepo := repo.NewTrackRepo(pgsqlDB, cfg)

		// initilizing usecases
		trackUseCases := usecases.NewTrackUseCases(trackRepo, cloud, cfg, queue)

		// initalizing controllers
		trackControllers := controllers.NewTrackController(api, trackUseCases)

		// init the routes
		trackControllers.InitRoutes()

	}

	// run the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	// common middlewares should be added here

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	fmt.Println("Server listening in...", cfg.Port)
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Printf("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown: %s", err.Error())
	}
	// catching ctx.Done(). timeout of 5 seconds.
	<-ctx.Done()
	log.Printf("timeout of 5 seconds.")

	log.Printf("Server exiting")
}

func awsInit(cfg *entities.EnvConfig) (*awsmanager.AwsConfig, error) {
	var optFns []func(*config.LoadOptions) error
	if !utils.IsEmpty(cfg.AWS.AccessKey) && !utils.IsEmpty(cfg.AWS.AccessSecret) {
		optFns = append(optFns, awsmanager.WithCredentialsProvider(cfg.AWS.AccessKey, cfg.AWS.AccessSecret))
	}
	if !utils.IsEmpty(cfg.AWS.Region) {
		optFns = append(optFns, awsmanager.WithRegion(cfg.AWS.Region))
	}
	awsConf, err := awsmanager.CreateAwsSession(optFns...)
	if err != nil {
		return nil, err
	}
	return awsConf, nil
}

server:
	go run main.go

test:
	go test -cover -v ./...

mock:
	mockgen -package mock -destination internal/repo/mock/track.go track/internal/repo TrackRepoImply
	mockgen -package mockcloud -destination internal/cloud/awsutils/mockcloud/track.go  track/internal/cloud/awsutils CloudServiceImply


package entities

import (
	"time"

	"gitlab.com/tuneverse/toolkit/models"
)

// Response represents a standard response structure for API responses.
type Response struct {
	StatusCode int              `json:"status_code"`
	Message    string           `json:"message"`
	MetaData   *models.MetaData `json:"meta_data,omitempty"`
	Data       interface{}      `json:"data,omitempty"`
}

// SuccessResponse represents a success response.
type SuccessResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// TrackMetadata represents metadata associated with a track.
type TrackMetadata struct {
	ID         string    `json:"id"`
	Name       string    `json:"name"`
	FileName   string    `json:"filename"`
	Duration   float64   `json:"duration"`
	CreatedOn  time.Time `json:"created_on"`
	UpdatedOn  time.Time `json:"updated_on"`
	MemberID   string    `json:"member_id"`
	Status     int16     `json:"status"`
	FileSize   int64     `json:"file_size"`
	FileExt    string    `json:"file_ext"`
	SampleRate int       `json:"sample_rate"`
	ISRC       string    `json:"isrc"`
}

package entities

// EnvConfig represents the configuration structure for the application.
type EnvConfig struct {
	Debug                  bool           `default:"true" split_words:"true"`  // Indicates whether the application is in debug mode (default: true)
	Port                   int            `default:"8080" split_words:"true"`  // The port on which the server listens (default: 8080)
	Db                     Database       `split_words:"true"`                 // Database configuration
	AcceptedVersions       []string       `required:"true" split_words:"true"` // List of accepted API versions (required)
	LocalisationServiceURL string         `split_words:"true"`                 // URL for the localization service
	LoggerServiceURL       string         `split_words:"true"`                 // URL for the logger service
	LoggerSecret           string         `split_words:"true"`                 // Secret key for logging
	EndpointURL            string         `split_words:"true"`                 // URL for the localization endpoint
	AWS                    AWS            `split_words:"true"`                 //  Aws configuration
	IsrcSeries             string         `split_words:"true"`
	MessageQueueType       string         `split_words:"true"`
	RabbitMQConfig         RabbitMQConfig `split_words:"true"`
	AudioFile              AudioFile
}

// Database represents the database configuration for the application.
type Database struct {
	User      string
	Password  string
	Port      int
	Host      string
	DATABASE  string
	Schema    string
	MaxActive int
	MaxIdle   int
}

// AWS represents AWS configuration settings, including access key, access secret, and region.
type AWS struct {
	AccessKey    string `split_words:"true"`
	AccessSecret string `split_words:"true"`
	Region       string
	BucketName   string `split_words:"true"`
	SqsQueueName string `split_words:"true"`
	SqsQueueUrl  string `split_words:"true"`
}

type RabbitMQConfig struct {
	User         string `split_words:"true"`
	Password     string `split_words:"true"`
	Host         string `split_words:"true"`
	Port         string `default:"5672" split_words:"true"`
	Exchange     string `split_words:"true"`
	Queue        string `split_words:"true"`
	RoutingKey   string `split_words:"true"`
	Durable      bool   `split_words:"true"`
	ExchangeType string `split_words:"true"`
	AutoDelete   bool   `split_words:"true"`
	Exclusive    bool   `split_words:"true"`
	NoWait       bool   `split_words:"true"`
	Mandatory    bool   `split_words:"true"`
	Immediate    bool   `split_words:"true"`
	NoLocal      bool   `split_words:"true"`
	AutoAck      bool   `split_words:"true"`
	Consumer     string `split_words:"true"`
}

type AudioFile struct {
	AcceptedSamplingRate int      `split_words:"true"`
	AcceptedFileFormats  []string `split_words:"true"`
	Size                 Range
	Duration             Range
}

type Range struct {
	Maximum float64
	Minimum float64
}

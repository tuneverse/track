package entities

import "github.com/google/uuid"

// Track represents a track entity, including its properties and associated data.
type Track struct {
	ID              string           `json:"id"`
	Name            string           `json:"name"`
	URL             string           `json:"url"`
	Explicit        int              `json:"explicit,omitempty"`
	Lyrics          string           `json:"lyrics,omitempty"`
	ISWC            string           `json:"iswc,omitempty"`
	ISRC            string           `json:"isrc,omitempty"`
	AlbumOnly       bool             `json:"album_only,omitempty"`
	AllowDownload   bool             `json:"allow_download,omitempty"`
	Status          string           `json:"status,omitempty"`
	MemberID        string           `json:"member_id,omitempty"`
	PartnerID       string           `json:"partner_id,omitempty"`
	Languages       Language         `json:"language,omitempty"`
	Participants    []Participant    `json:"participants,omitempty"`
	FeaturedArtists []FeaturedArtist `json:"featured_artists,omitempty"`
	Genre           []Genre          `json:"genres,omitempty"`
	Artists         []Artist         `json:"artists,omitempty"`
}

// Language represents language information for a track.
type Language struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Code string `json:"code"`
}

// Participant represents an individual involved in a track, such as a performer or contributor.
type Participant struct {
	ID         uuid.UUID `json:"id"`
	Name       string    `json:"name"`
	RoleID     uuid.UUID `json:"role_id,omitempty"`
	Role       string    `json:"role,omitempty"`
	Percentage int       `json:"percentage,omitempty"`
}

// FeaturedArtist represents an artist featured in a track.
type FeaturedArtist struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Genre represents a musical genre.
type Genre struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// Artist represents an artist associated with a track.
type Artist struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type ISRC struct {
	ID            int
	Series        string
	FirstValue    int
	LastValue     int
	LastUsedValue int
	PartnerID     string
}

type UnusedISRCode struct {
	ID     int
	Series string
	Code   int
	Year   int
}

type UpdateTrackStatusRequest struct {
	TrackID       string `json:"track_id"`
	MemberID      string `json:"member_id"`
	PartnerID     string `json:"partner_id"`
	Status        int    `json:"status"`
	FileSize      int    `json:"file_size"`
	FileExtension string `json:"file_extension"`
	SampleRate    int    `json:"samplerate"`
}

type MessageBody struct {
	TaskType string
	Payload  map[string]interface{}
}

type QueueMessage struct {
	EventID       string
	TrackID       string
	MemberID      string
	PartnerID     string
	FileExtension string
}

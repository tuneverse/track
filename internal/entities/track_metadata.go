package entities

import (
	"database/sql"
	"encoding/json"
	"track/internal/consts"
	"track/utilities"

	"github.com/google/uuid"
)

// TrackProperties represents the metadata of a track.
type TrackProperties map[string]any

// GetTrackName returns the name of the track.
func (x TrackProperties) GetTrackName() string {
	if name, ok := x["name"].(string); ok {
		return name
	}
	return ""
}

// GetLangCode returns the language code of the track.
func (x TrackProperties) GetLangCode() string {
	if language, ok := x["language"].(string); ok {
		return language
	}
	return ""
}

// GetISRC returns the ISRC code of the track.
func (x TrackProperties) GetISRC() string {
	if isrc, ok := x["isrc"].(string); ok {
		return isrc
	}
	return ""
}

// GetISWC returns the ISWC code of the track.
func (x TrackProperties) GetISWC() string {
	if iswc, ok := x["iswc"].(string); ok {
		return iswc
	}
	return ""
}

// GetExplicit returns the explicit flag of the track.
func (x TrackProperties) GetExplicit() int16 {
	if explicit, ok := x["explicit"].(float64); ok {
		return int16(explicit)
	}
	return 0
}

// GetAllowDownload returns the allow download flag of the track.
func (x TrackProperties) GetAllowDownload() bool {
	if allowDownload, ok := x["allow_download"].(bool); ok {
		return allowDownload
	}
	return false
}

// GetAlbumOnly returns the album only flag of the track.
func (x TrackProperties) GetAlbumOnly() bool {
	if albumOnly, ok := x["album_only"].(bool); ok {
		return albumOnly
	}
	return false
}

// GetPreviewStartAt returns the preview start time of the track.
func (x TrackProperties) GetPreviewStartAt() int32 {
	if previewStartAt, ok := x["preview_start_at"].(float64); ok {
		return int32(previewStartAt)
	}
	return 0
}

// GetPreviewDuration returns the preview duration of the track.
func (x TrackProperties) GetPreviewDuration() int32 {
	if previewDuration, ok := x["preview_duration"].(float64); ok {
		return int32(previewDuration)
	}
	return 0

}

// GetLyrics returns the lyrics of the track.
func (x TrackProperties) GetLyrics() string {
	if lyrics, ok := x["lyrics"].(string); ok {
		return lyrics
	}
	return ""
}

// GetIsInstrumental returns the is instrumental flag of the track.
func (x TrackProperties) GetIsInstrumental() bool {
	if isInstrumental, ok := x["is_instrumental"].(bool); ok {
		return isInstrumental
	}
	return false

}

// GetTrackID returns the ID of the track.
func (x TrackProperties) GetTrackID() string {
	if id, ok := x["id"].(string); ok {
		return id
	}
	return ""
}

// GetMemberID returns the id of the member.
func (x TrackProperties) GetMemberID() string {
	if id, ok := x["member_id"].(string); ok {
		return id
	}
	return ""
}

// ParseTrackID parses the track id
func (x TrackProperties) ParseTrackID() error {
	trackID := x.GetTrackID()
	trackUUID, err := uuid.Parse(trackID)
	if err != nil {
		return err
	}
	x["id"] = trackUUID.String()
	return nil
}

// GetGenres returns the genres associated with the track
func (x TrackProperties) GetGenres() ([]uuid.UUID, error) {
	var (
		genres []uuid.UUID
		err    error
	)
	if v, ok := x["genres"].([]any); ok {
		if len(v) == 0 {
			return nil, nil
		}
		genres, err = utilities.ConvertSliceToUUIDs(v)
		if err != nil {
			return nil, err
		}
	}
	return genres, nil
}

// GetArtists returns the artists associated with the track
func (x TrackProperties) GetArtists() ([]uuid.UUID, []uuid.UUID, error) {
	var (
		artists, featuredArtist []uuid.UUID
		err                     error
	)
	if v, ok := x["artists"].([]any); ok {
		if len(v) == 0 {
			return nil, nil, nil
		}
		artists, featuredArtist, err = GetArtist(v)
		if err != nil {
			return nil, nil, err
		}
	}
	return artists, featuredArtist, nil
}

func GetArtist(arg []interface{}) ([]uuid.UUID, []uuid.UUID, error) {
	var artistIds, featuredArtistIds []uuid.UUID
	for _, item := range arg {
		obj, ok := item.(map[string]interface{})
		if !ok {
			return nil, nil, consts.ErrInvalidUUID
		}
		artistId, ok := obj[consts.ID].(string)
		if !ok {
			return nil, nil, consts.ErrInvalidUUID
		}

		isFeature, ok := obj[consts.IsFeatureFlag].(bool)
		if !ok {
			return nil, nil, consts.ErrInvalidUUID
		}

		uuidValue, err := uuid.Parse(artistId)
		if err != nil {
			return nil, nil, err
		}
		if isFeature {
			featuredArtistIds = append(featuredArtistIds, uuidValue)
		} else {
			artistIds = append(artistIds, uuidValue)
		}
	}
	return artistIds, featuredArtistIds, nil
}

// GetParticipants returns the participants associated with the track
func (x TrackProperties) GetParticipants() ([]Participant, error) {
	var participants []Participant
	if v, ok := x["participants"].([]any); ok {
		for _, k := range v {

			p, ok := k.(map[string]any)
			if !ok {
				return nil, consts.ErrInvalidParticipant
			}
			var participant Participant
			bsofParticipant, err := json.Marshal(p)
			if err != nil {
				return nil, consts.ErrInvalidParticipant
			}
			if err := json.Unmarshal(bsofParticipant, &participant); err != nil {
				return nil, consts.ErrInvalidParticipant
			}
			participants = append(participants, participant)
		}
	}

	return participants, nil
}

// KeyExists checks whether key is available in the map
func (x TrackProperties) KeyExists(key string) bool {
	if _, ok := x[key]; ok {
		return true
	}
	return false
}

// UpdateTrackParams represents the parameters for updating a track.
type UpdateTrackParams struct {
	Name            sql.NullString `json:"name"`
	LanguageCode    sql.NullString `json:"language_code,omitempty"`
	Explicit        sql.NullInt16  `json:"explicit,omitempty"`
	AlbumOnly       sql.NullBool   `json:"album_only"`
	ISWC            sql.NullString `json:"iswc,omitempty"`
	Lyrics          sql.NullString `json:"lyrics,omitempty"`
	PreviewStartAt  sql.NullInt32  `json:"preview_start_at,omitempty"`
	PreviewDuration sql.NullInt32  `json:"preview_duration,omitempty"`
	IsInstrumental  sql.NullBool   `json:"is_instrumental,omitempty"`
	AllowDownload   sql.NullBool   `json:"allow_download,omitempty"`
	ISRC            sql.NullString `json:"isrc,omitempty"`
}

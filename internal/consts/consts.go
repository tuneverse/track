package consts

import (
	"errors"
)

// Constants defining fundamental properties and settings of the application.
const (
	DatabaseType = "postgres"
	AppName      = "track"
)

// Context setting keys used to manage and access specific values within the application context.
const (
	ContextEndPoints = "context-endpoints"
)

// Cache Keys
const (
	CacheErrorKey     = "ERROR_CACHE_KEY_LABEL"
	CacheEndpointsKey = "endpoints"
)

// Status represents the status of a task or operation.
type status int

// Constants defining values to status
const (
	Started status = iota + 1
	Completed
	Queued
	Success
	Failed
)

// IsrcSeries is the prefix for ISRC (International Standard Recording Code) series.
const (
	IsrcSeries = "inxyz"
)

// Constants defining fundamental properties and settings for file size, duration, sampling rate, directories, and content type.
const (
	// MinFileSizeMb is the minimum file size allowed in MB.
	MinFileSizeMb = 1
	// MaxFileSizeMb allowed in MB (1 GB = 1024 MB)
	MaxFileSizeMb = 1024
	// MinimumDuration is the minimum duration allowed in seconds.
	MinimumDuration = 30
	// MinSamplingRateHz is the minimum allowed audio sample rate in Hertz.
	MinSamplingRateHz = 44100
	// DefaultContentType is the default content type for files.
	DefaultContentType = "application/octet-stream"
)

// NotFound is an error label indicating that a resource was not found.
var (
	ResourceNotFound = "not_found"
)

// keys to retrive error
const (
	Required          = "required"
	Length            = "length"
	GenreCount        = "genre_count"
	Invalid           = "invalid"
	NotExists         = "not_exists"
	NoRoles           = "no_roles"
	NotFound          = "not_found"
	InvalidPercentage = "invalid_percentage"
	ArtistCount       = "artist_count"
	ParticipantCount  = "participant_count"
	InvalidInput      = "common_msg:invalid_input"
	InvalidData       = "invalid_data"
)

// field names to retrive error
const (
	TrackName       = "name"
	TrackGenres     = "genres"
	ISRC            = "isrc"
	ISWC            = "iswc"
	TrackLanguage   = "language"
	TrackExplicit   = "explicit"
	Artists         = "artists"
	FeaturedArtists = "featured_artists"
	Participants    = "participants"
	LangCode        = "code"
)

const (
	ID            = "id"
	IsFeatureFlag = "is_featured"
)

// Track fields
var Track = map[string]bool{
	"id":               true,
	"name":             true,
	"genres":           true,
	"isrc":             true,
	"iswc":             true,
	"language":         true,
	"explicit":         true,
	"artists":          true,
	"featured_artists": true,
	"participants":     true,
	"album_only":       true,
	"lyrics":           true,
	"preview_start_at": true,
	"preview_duration": true,
	"is_instrumental":  true,
	"member_id":        true,
	"allow_download":   true,
	"code":             true,
}

// errors
var (
	ErrNotFound           = errors.New("not found")
	ErrInvalidInput       = errors.New("invalid input params")
	ErrInvalidUUID        = errors.New("inavlid uuid")
	ErrInvalidParticipant = errors.New("invalid participant")
)

// Table names
const (
	TableTrackArtist       = "track_artist"
	TableTrackArtistRole   = "track_artist_role"
	TableFeaturedArtist    = "track_featured_artist"
	TableTrackGenre        = "track_genre"
	TableLanguage          = "language"
	TableArtist            = "artist"
	TablePartnerGenres     = "partner_genre_language"
	TablePartnerArtistRole = "partner_artist_role_language"
)

// s3 expiration
const (
	Expiration = 2
)

const (
	SystemPartnerID = "96b05633-69d7-4144-b0f9-73765b9ea7ca"
)

package mockqueue

import "context"

// MockQueue is a mock implementation of the Queue interface.
type MockQueue struct {
	ComposeFunc func(ctx context.Context, message []byte) (interface{}, error)
	SendFunc    func(ctx context.Context, message interface{}) error
	ReceiveFunc func(ctx context.Context) (interface{}, error)
	DeleteFunc  func(ctx context.Context, receiptHandle string) error
	CloseFunc   func() error
}

// ComposeMessage mocks the ComposeMessage method.
func (mq *MockQueue) ComposeMessage(ctx context.Context, message []byte) (interface{}, error) {
	if mq.ComposeFunc != nil {
		return mq.ComposeFunc(ctx, message)
	}
	return nil, nil
}

// Send mocks the Send method.
func (mq *MockQueue) Send(ctx context.Context, message interface{}) error {
	if mq.SendFunc != nil {
		return mq.SendFunc(ctx, message)
	}
	return nil
}

// Receive mocks the Receive method.
func (mq *MockQueue) Receive(ctx context.Context) (interface{}, error) {
	if mq.ReceiveFunc != nil {
		return mq.ReceiveFunc(ctx)
	}
	return nil, nil
}

// Delete mocks the Delete method.
func (mq *MockQueue) Delete(ctx context.Context, receiptHandle string) error {
	if mq.DeleteFunc != nil {
		return mq.DeleteFunc(ctx, receiptHandle)
	}
	return nil
}

// Close mocks the Close method.
func (mq *MockQueue) Close() error {
	if mq.CloseFunc != nil {
		return mq.CloseFunc()
	}
	return nil
}

package repo

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
	"track/internal/consts"
	"track/internal/entities"
	"track/utilities"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

// TrackRepo is responsible for handling track-related data.
type TrackRepo struct {
	db  *sql.DB
	env *entities.EnvConfig
}

// TrackRepoImply is the interface defining the methods for working with Track data.
type TrackRepoImply interface {
	GetISRCByTrackID(ctx context.Context, trackId string) (string, error)
	UpdateIsrcStatus(ctx context.Context, isrc string) error
	ValidateTrackOwnership(ctx context.Context, trackId, memberId string) error
	GetFileExtension(ctx context.Context, trackId, memberId string) (string, error)
	DeleteTrack(ctx context.Context, trackID string) error
	MarkTrackAsDeleted(ctx context.Context, trackID string) error
	CheckTrackIsAssignedToProduct(ctx context.Context, trackId string) error
	GetTrack(context.Context, string) (entities.Track, error)
	UpdateTrackMetadata(ctx context.Context, metadata entities.TrackMetadata) error
	ValidateTrackOwner(ctx context.Context, trackID, memberID string) (bool, error)
	CountTrackMember(ctx context.Context, trackID, memberID string) (int, error)
	InsertTrackMetadata(ctx context.Context, metadata entities.TrackMetadata) (string, error)
	UpdateTrackStatus(ctx context.Context, userID string, trackID string, status int) error
	GenerateISRC(ctx context.Context, partnerID string) (ISRC string, err error)
	GetID(ctx context.Context, tableName, field, fieldValue, returnField string) (any, error)
	ExecuteTx(ctx context.Context, fn func(tx *sql.Tx) error) error
	DeleteTrackProperties(ctx context.Context, trackID string, tableName string) error
	InsertTrackArtists(ctx context.Context, trackID string, artistIDs ...uuid.UUID) error
	InsertTrackParticipants(ctx context.Context, trackID string, participants ...entities.Participant) error
	InsertTrackGenres(ctx context.Context, trackID string, genreIDs ...uuid.UUID) error
	InsertFeaturedArtists(ctx context.Context, trackID string, genreIDs ...uuid.UUID) error
	VerifyTrackGenres(ctx context.Context, partnerID string, genreIDs ...uuid.UUID) ([]uuid.UUID, error)
	VerifyArtists(ctx context.Context, memeberID string, artistIDs ...uuid.UUID) ([]uuid.UUID, error)
	UpdateTrackInfo(ctx context.Context, params entities.UpdateTrackParams, trackID string) (int64, error)
	UpdateTrackTx(ctx context.Context, arg entities.TrackProperties) (int64, error)
	VerifyRoles(ctx context.Context, partnerID string, roleIDs ...uuid.UUID) ([]uuid.UUID, error)
	VerifyIDs(ctx context.Context, tableName, columnName, condition, entityID string, ids ...uuid.UUID) ([]uuid.UUID, error)
	UpdateConvertionStatus(ctx context.Context, userID string, trackID string, reqbody entities.UpdateTrackStatusRequest) error
}

// NewTrackRepo creates a new TrackRepo instance.
func NewTrackRepo(db *sql.DB, env *entities.EnvConfig) TrackRepoImply {
	return &TrackRepo{db: db, env: env}
}

// InsertTrackMetadata inserts track metadata into the repository and returns the track's unique ID.
func (repo *TrackRepo) InsertTrackMetadata(ctx context.Context, metadata entities.TrackMetadata) (string, error) {
	var insertedID string

	// Execute the SQL query with context and retrieve the inserted ID
	err := repo.db.QueryRowContext(ctx, `
        INSERT INTO track (id, name, filename, duration, created_on, member_id, status, file_size, file_ext, sample_rate, isrc)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
        RETURNING id`,
		metadata.ID, metadata.Name, metadata.FileName, metadata.Duration, metadata.CreatedOn,
		metadata.MemberID, metadata.Status, metadata.FileSize, metadata.FileExt, metadata.SampleRate, metadata.ISRC,
	).Scan(&insertedID)

	if err != nil {
		return "", fmt.Errorf("failed to insert track metadata: %v", err)
	}

	return insertedID, nil
}

func (repo *TrackRepo) UpdateTrackStatus(ctx context.Context, userID string, trackID string, status int) error {
	_, err := repo.db.ExecContext(ctx, "UPDATE track SET status = $1 WHERE member_id = $2 AND id = $3", status, userID, trackID)
	log := logger.Log().WithContext(ctx)
	if err != nil {
		if ctx.Err() == context.Canceled {
			// Handle context cancellation error
			log.Errorf("Repository: Context canceled during database operation, %s", err.Error())
			return ctx.Err()
		}
		log.Errorf("Repository: Failed to update track status: %v", err)
		return err
	}
	return nil
}

func (repo *TrackRepo) GetISRCDetails(ctx context.Context, partnerID string) ([]entities.ISRC, error) {
	ISRCs := make([]entities.ISRC, 0)

	systemPartnerID, err := uuid.Parse(consts.SystemPartnerID)
	if err != nil {
		return ISRCs, err
	}

	getISRCDataQ := `select id,series,first_value,last_value,last_used_value,partner_id from isrc 
					where (partner_id = $1
		  				and is_default= $2)
					OR (partner_id = $3
		  				and is_default= $4)`

	rows, err := repo.db.QueryContext(ctx, getISRCDataQ, systemPartnerID, true, partnerID, true)

	if err != nil {
		return ISRCs, err
	}

	defer rows.Close()

	for rows.Next() {
		var ISRC entities.ISRC
		err := rows.Scan(
			&ISRC.ID,
			&ISRC.Series,
			&ISRC.FirstValue,
			&ISRC.LastValue,
			&ISRC.LastUsedValue,
			&ISRC.PartnerID,
		)

		if err != nil {
			return ISRCs, err
		}

		ISRCs = append(ISRCs, ISRC)
	}

	if err := rows.Err(); err != nil {
		return ISRCs, err
	}

	return ISRCs, nil
}

// GetUnusedISRC returns the details of the smallest unused ISRC
// such as id,series,first_value,last_value,last_used_value,partner_id from the database.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@partnerID: Unique ID to identify the partner
//
// Returns:
//
//	@ISRCData: A slice of entities.ISRC, each representing an ISRC data
//	@err: An error, if any, encountered during the database operation.
func (repo *TrackRepo) GetUnusedISRC(ctx context.Context, series string, code int) (entities.UnusedISRCode, error) {

	getUnusedISRCQ := `select id,series,year,code from isrc_generated_code 
	where series = $1 and code <= $2 and is_used = $3 order by code asc limit 1 `

	row := repo.db.QueryRowContext(ctx, getUnusedISRCQ, series, code, false)

	var unusedISRCode entities.UnusedISRCode
	err := row.Scan(
		&unusedISRCode.ID,
		&unusedISRCode.Series,
		&unusedISRCode.Year,
		&unusedISRCode.Code,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return unusedISRCode, nil
		}
		return unusedISRCode, err
	}

	return unusedISRCode, nil
}

// GenerateISRC returns the next usable ISRC
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@partnerID: Unique ID to identify the partner
//
// Returns:
//
//	@ISRC: ISRC stands for International Standard Recording Code.
//  	It is a unique identifier for audio and music video recordings.
//  	Each ISRC is a 12-character alphanumeric code that helps identify and differentiate specific recordings from one another.
// 		This code is used in the music industry to track and manage digital music files, facilitate royalty collection,
//		and ensure proper distribution and recognition of individual audio tracks.
// 		Here is an example ISRC code format: USL8I2312345
// 			US - represents the United States (the country code).
// 			L8I - represents the registrant code.
// 			23 - represents the year of reference.
// 			12345 - is the designation code.
//	@err: An error, if any, encountered during the database operation.

func (repo *TrackRepo) GenerateISRC(ctx context.Context, partnerID string) (ISRC string, err error) {
	var (
		ISRCData entities.ISRC
	)

	ISRCDetails, _ := repo.GetISRCDetails(ctx, partnerID)
	if len(ISRCDetails) > 1 { // Use partner ISRC
		if ISRCDetails[0].PartnerID == partnerID { // If partner specific ISRC is configured
			ISRCData = ISRCDetails[0]
		} else { // Use default system ISRC
			ISRCData = ISRCDetails[1]
		}
	} else if len(ISRCDetails) == 1 { // Use default system ISRC
		ISRCData = ISRCDetails[0]
	} else {
		return ISRC, errors.New("no valid ISRC series exists")
	}

	emptyUnusedISRCode := entities.UnusedISRCode{}

	//fetch smallest unused ISRC
	unusedISRCode, err := repo.GetUnusedISRC(ctx, ISRCData.Series, ISRCData.LastUsedValue)

	if err != nil {
		return ISRC, err
	}

	if unusedISRCode != emptyUnusedISRCode { //Unused ISRCs available to use

		//Mark 'is_used' flag to true in 'isrc_generated_code' table as it is now in use
		updateUsedValueStatusQ := `update isrc_generated_code set is_used = $1
										where id = $2`

		_, err = repo.db.Exec(updateUsedValueStatusQ, true, unusedISRCode.ID)

		// Return any error encountered during the database operation.
		if err != nil {
			return ISRC, err
		}

		//Mask the designation code to 5 digit
		designationCode := fmt.Sprintf("%05d", unusedISRCode.Code)
		//Generate the ISRC compaining Series(country code + registrant code),year and designation code
		ISRC = unusedISRCode.Series + strconv.Itoa(unusedISRCode.Year) + designationCode
	} else { //No Unused ISRCs available, so generate a new ISRC
		nextValue := ISRCData.LastUsedValue + 1
		//Check nextValue is less than 'LastValue' feild in 'isrc' table
		if nextValue <= ISRCData.LastValue {
			//Mask the designation code to 5 digit
			designationCode := fmt.Sprintf("%05d", nextValue)
			currentYear := time.Now().Year() % 100
			//Generate the ISRC compaining Series(country code + registrant code),year and designation code
			ISRC = ISRCData.Series + strconv.Itoa(currentYear) + designationCode

			//Insert the details of newly generated ISRC details to the 'isrc_generated_code' table
			insertISRCGeneratedCodeQ := `INSERT INTO isrc_generated_code
										(series, year, code, is_used)
										VALUES ( $1, $2, $3, $4)`

			_, err = repo.db.Exec(insertISRCGeneratedCodeQ, ISRCData.Series, currentYear, nextValue, true)

			if err != nil {
				return ISRC, err
			}

			//Update the 'last_used_value' feild of the corresponding ISRC series in 'isrc' table
			updateLastUsedValueQ := `update isrc set last_used_value = last_used_value + 1 where id = $1`
			_, err = repo.db.Exec(updateLastUsedValueQ, ISRCData.ID)

			if err != nil {
				return ISRC, err
			}
		} else {
			return ISRC, errors.New("The ISRC threshold reached for the active series ," + ISRCData.Series)
		}
	}
	return ISRC, nil
}

// DeleteTrack deletes a track and performs a series of database operations to remove
// the track and its associated data.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackID: The unique identifier for the track to be deleted.
//
// Functionality:
//   - Begins a database transaction.
//   - Deletes records associated with the specified track from various tables.
//   - Commits the transaction on success or rolls back on error.
//
// Returns:
//
//	An error if any database operation encounters an issue, or nil if the deletion is successful.
func (track *TrackRepo) DeleteTrack(ctx context.Context, trackID string) error {
	tx, err := track.db.BeginTx(ctx, nil)
	if err != nil {
		return handleError(ctx, "DeleteTrack", "BeginTx", err, tx)
	}
	defer func() {
		if err != nil {
			handleError(ctx, "DeleteTrack", "Rollback", tx.Rollback(), nil)
			return
		}
		handleError(ctx, "DeleteTrack", "Commit", tx.Commit(), nil)
	}()

	tablesToDeleteFrom := []struct {
		name         string
		conditionKey string
	}{
		{"track", "id"},
		{"track_artist_role", "track_id"},
		{"track_artist", "track_id"},
		//{"track_featured_artist", "track_id"},
		{"track_genre", "track_id"},
	}

	for _, table := range tablesToDeleteFrom {
		err = DeleteFromTableByConditionWithTransaction(ctx, tx, table.name, table.conditionKey, trackID)
		if err != nil {
			logger.Log().WithContext(ctx).WithFields(map[string]interface{}{"table_name": table.name, "column_name": table.conditionKey}).Errorf("DeleteFromTableByConditionWithTransaction table %s, condition field %s, value %v failed, Exec() failed, err=%s", table.name, table.conditionKey, trackID, err.Error())
			return handleError(ctx, "DeleteTrack", "DeleteFromTableByConditionWithTransaction", err, tx)
		}
	}

	return nil
}

// DeleteFromTableByConditionWithTransaction is a utility function to delete records from a specific table
// based on a condition within a transaction.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@tx: The active database transaction.
//	@tablename: The name of the table to delete records from.
//	@conditionField: The field used as the condition for deletion.
//	@value: The value of the condition field to match for deletion.
//
// Functionality:
//   - Constructs a DELETE query to remove records from the specified table based on the provided condition.
//   - Executes the DELETE query within the transaction.
//
// Returns:
//
//	An error if the DELETE operation encounters an issue, or nil on success.
func DeleteFromTableByConditionWithTransaction(ctx context.Context, tx *sql.Tx, tablename string, conditionField string, value interface{}) error {
	query := fmt.Sprintf(`DELETE FROM %s WHERE %s = $1`, tablename, conditionField)
	_, err := tx.ExecContext(ctx, query, value)
	if err != nil {
		return handleError(ctx, "DeleteFromTableByConditionWithTransaction", "Exec", err, nil)
	}
	return nil
}

// handleError is a utility function to handle errors during database operations and transactions.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@function: The name of the function where the error occurred.
//	@action: The specific action within the function.
//	@err: The error that needs to be handled.
//	@tx: The database transaction (if available).
//
// Functionality:
//   - Logs the error and any additional information.
//   - Rolls back the transaction (if provided) in case of an error.
//
// Returns:
//
//	The original error for further handling or reporting.
func handleError(ctx context.Context, function, action string, err error, tx *sql.Tx) error {
	if err != nil {
		if tx != nil {
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				logger.Log().WithContext(ctx).Errorf("%s failed: %s, %s rollback failed: %s", function, action, err, rollbackErr)
			}
		}
		logger.Log().WithContext(ctx).Errorf("%s failed: %s, action: %s", function, err, action)
		return err
	}
	return nil
}

// MarkTrackAsDeleted updates the 'is_deleted' flag for a track, marking it as deleted
// and setting the 'deleted_on' timestamp to the current time.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackID: The unique identifier for the track to mark as deleted.
//
// Returns:
//
//	An error if the database operation encounters an issue, or nil on success.
func (track *TrackRepo) MarkTrackAsDeleted(ctx context.Context, trackID string) error {
	_, err := track.db.ExecContext(ctx, "UPDATE track SET is_deleted = true, deleted_on = NOW() WHERE id = $1", trackID)
	if err != nil {
		return err
	}
	return nil
}

// CheckTrackIsAssignedToProduct checks whether a track is assigned to any product
// by counting the number of occurrences of the track's ID in the 'product_track' table.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackId: The unique identifier for the track.
//
// Returns:
//
//	An error if the database operation encounters an issue or if the track is assigned to a product.
//	Returns 'trackid is assigned to products' error if the count is greater than zero.
func (track *TrackRepo) CheckTrackIsAssignedToProduct(ctx context.Context, trackId string) error {
	query := "SELECT COUNT(*) FROM product_track WHERE track_id=$1"
	var count int
	err := track.db.QueryRowContext(ctx, query, trackId).Scan(&count)
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New("trackid is assigned to products")
	}
	return nil
}

// ValidateTrackOwnership validates the ownership of a track by checking whether the
// specified member (by ID) owns the track (by ID).
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackId: The unique identifier for the track.
//	@memberId: The unique identifier for the member.
//
// Returns:
//
//	An error if the database operation encounters an issue or if the member doesn't own the track.
//	Returns 'memberId and trackId pair not found' error if the ownership is not validated.
func (track *TrackRepo) ValidateTrackOwnership(ctx context.Context, trackId, memberId string) error {
	query := "SELECT COUNT(*) FROM track WHERE member_id=$1 AND id=$2"
	var count int
	err := track.db.QueryRowContext(ctx, query, memberId, trackId).Scan(&count)
	if err != nil {
		fmt.Println("repo error", err)
		return err
	}

	if count == 0 {
		return errors.New("memberId and trackId pair not found")
	}
	return nil
}

// GetFileExtension retrieves the file extension of a track by its ID and member ID.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackId: The unique identifier for the track.
//	@memberId: The unique identifier for the member.
//
// Returns:
//
//	The file extension as a string if found, or an error if the database operation encounters an issue.
func (track *TrackRepo) GetFileExtension(ctx context.Context, trackId, memberId string) (string, error) {
	query := "SELECT file_ext FROM track WHERE member_id=$1 AND id=$2"
	var ext string
	err := track.db.QueryRowContext(ctx, query, memberId, trackId).Scan(&ext)
	if err != nil {
		fmt.Println("repo error", err)
		return "", err
	}
	return ext, nil
}

// GetISRCByTrackID retrieves the ISRC (International Standard Recording Code) of a track by its ID.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackId: The unique identifier for the track.
//
// Returns:
//
//	The ISRC as a string if found, or an error if the database operation encounters an issue.
func (repo *TrackRepo) GetISRCByTrackID(ctx context.Context, trackId string) (string, error) {
	query := "SELECT isrc FROM track WHERE id = $1"

	var isrc string
	err := repo.db.QueryRowContext(ctx, query, trackId).Scan(&isrc)
	if err != nil {
		return "", err
	}

	return isrc, nil
}

// UpdateIsrcStatus updates the status of an ISRC code in the database.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@isrc: The ISRC code to update.
//
// Returns:
//
//	An error if the database operation encounters an issue, or nil on success.
func (repo *TrackRepo) UpdateIsrcStatus(ctx context.Context, isrc string) error {
	// Logic to update the track status in the database
	series, year, code, err := splitISRC(isrc)
	if err != nil {
		fmt.Println("repo error", err)
		return err
	}
	_, err = repo.db.ExecContext(ctx, "UPDATE isrc_generated_code SET is_used = $1 WHERE series = $2 AND year = $3 AND code = $4", false, series, year, code)
	if err != nil {
		return fmt.Errorf("failed to update track status: %v", err)
	}
	return nil
}

// splitISRC extracts components (series, year, and code) from an ISRC (International Standard Recording Code).
//
// Parameters:
//
//	@isrc: The ISRC code to split.
//
// Returns:
//   - series: The series part of the ISRC (converted to lowercase).
//   - year: The year part of the ISRC as an integer.
//   - code: The code part of the ISRC as an integer.
//   - err: An error if the ISRC format is invalid, or nil if the split is successful.
func splitISRC(isrc string) (series string, year int, code int, err error) {
	// Define a regular expression to match the ISRC pattern
	regex := regexp.MustCompile(`^([A-Z]{2}[A-Z0-9]{3})(\d{2})(\d{5})$`)

	// Use the regular expression to extract the components
	matches := regex.FindStringSubmatch(isrc)
	if len(matches) == 4 {
		series = strings.ToLower(matches[1]) // Convert to lowercase
		year, _ = strconv.Atoi(matches[2])
		code, _ = strconv.Atoi(matches[3])
		return series, year, code, nil
	}

	return "", 0, 0, fmt.Errorf("invalid ISRC format: %s", isrc)
}

// ExecuteTx executes a transaction on the database.
// It takes a context and a function that takes a transaction and returns an error.
// It returns an error if the transaction fails to begin, rollback, or commit.
func (trackRepo *TrackRepo) ExecuteTx(ctx context.Context, fn func(tx *sql.Tx) error) error {

	lg := logger.Log().WithContext(ctx)

	// Begin a db transaction with the provided context.
	tx, err := trackRepo.db.BeginTx(ctx, nil)
	if err != nil {
		lg.Errorf("ExecuteTx: Failed to begin transaction: %s", err.Error())
		return err
	}

	if err := fn(tx); err != nil {
		if rbErr := tx.Rollback(); rbErr != nil {
			// Log the error if a rollback operation fails.
			lg.Errorf("ExecuteTx: Rollback failed: %s", rbErr.Error())
			return fmt.Errorf("tx error %v, rbErr %v", err, rbErr)
		}
		return err
	}

	if err := tx.Commit(); err != nil {
		// Log the error if the commit operation fails.
		lg.Errorf("ExecuteTx: Commit failed: %s", err.Error())
		return err
	}

	return nil
}

// DeleteTrackProperties deletes specific properties of a track from the provided table in the database.
func (trackRepo *TrackRepo) DeleteTrackProperties(ctx context.Context, trackID string, tableName string) error {

	lg := logger.Log().WithContext(ctx)

	sql := `
		DELETE FROM %s
		WHERE track_id=$1
	`
	sql = fmt.Sprintf(sql, tableName)
	_, err := trackRepo.db.Exec(sql, trackID)
	if err != nil {
		lg.Errorf("DeleteTrackProperties: Database execution error: %s", err.Error())
		return err
	}
	return nil
}

// InsertTrackArtists inserts a track's artists into the database.
func (trackRepo *TrackRepo) InsertTrackArtists(ctx context.Context, trackID string, artistIDs ...uuid.UUID) error {

	lg := logger.Log().WithContext(ctx)

	sql := `
		INSERT INTO track_artist(track_id, artist_id) VALUES
	`
	args := make([]interface{}, 0)
	args = append(args, trackID)
	count := 1
	for _, artistID := range artistIDs {
		count++
		sql += "($1, $%d), "
		sql = fmt.Sprintf(sql, count)
		args = append(args, artistID)
	}
	sql = strings.TrimSuffix(sql, ", ")
	_, err := trackRepo.db.ExecContext(ctx, sql, args...)
	if err != nil {
		lg.Errorf("InsertTrackArtists: Database execution error: %s", err.Error())
		return err
	}
	return nil
}

// InsertFeaturedArtists inserts featured artists for a track into the database.
func (trackRepo *TrackRepo) InsertFeaturedArtists(ctx context.Context, trackID string, featuredArtIds ...uuid.UUID) error {

	lg := logger.Log().WithContext(ctx)

	sql := `
		INSERT INTO track_artist(track_id, artist_id,is_featured) VALUES
	`
	args := make([]interface{}, 0)
	args = append(args, trackID)
	count := 1
	for _, artistID := range featuredArtIds {
		count++
		sql += "($1, $%d, true), "
		sql = fmt.Sprintf(sql, count)
		args = append(args, artistID)
	}
	sql = strings.TrimSuffix(sql, ", ")
	_, err := trackRepo.db.ExecContext(ctx, sql, args...)
	if err != nil {
		lg.Errorf("InsertFeaturedArtists: Database execution error: %s", err.Error())
		return err
	}
	return nil
}

// InsertTrackParticipants inserts participants associated with a track into the database.
func (trackRepo *TrackRepo) InsertTrackParticipants(ctx context.Context, trackID string, participants ...entities.Participant) error {

	lg := logger.Log().WithContext(ctx)

	sql := `
		INSERT INTO track_artist_role(track_id, artist_id, role_id, percentage) VALUES
	`
	args := make([]interface{}, 0)
	args = append(args, trackID)
	count := 2
	for _, participant := range participants {
		sql += " ($1, $%d, $%d, $%d), "
		sql = fmt.Sprintf(sql, count, count+1, count+2)
		args = append(args, participant.ID, participant.RoleID, participant.Percentage)
		count += 3
	}

	sql = strings.TrimSuffix(sql, ", ")
	_, err := trackRepo.db.ExecContext(ctx, sql, args...)
	if err != nil {
		lg.Errorf("InsertTrackParticipants: Database execution error: %s", err.Error())
		return err
	}
	return nil
}

// InsertTrackGenres inserts the genres associated with a track into the database.
func (trackRepo *TrackRepo) InsertTrackGenres(ctx context.Context, trackID string, genreIDs ...uuid.UUID) error {

	lg := logger.Log().WithContext(ctx)

	sql := `
		INSERT INTO track_genre(track_id, genre_id) VALUES
	`
	args := make([]interface{}, 0)
	args = append(args, trackID)
	count := 1
	for _, generID := range genreIDs {
		count++
		sql += " ($1, $%d), "
		sql = fmt.Sprintf(sql, count)
		args = append(args, generID)
	}

	sql = strings.TrimSuffix(sql, ", ")
	_, err := trackRepo.db.ExecContext(ctx, sql, args...)
	if err != nil {
		lg.Errorf("InsertTrackGenres: Database execution error: %s", err.Error())
		return err
	}
	return nil
}

// VerifyIDs is a generic function for verifying IDs associated with a specific entity.
func (trackRepo *TrackRepo) VerifyIDs(ctx context.Context, tableName, columnName, condition, entityID string, ids ...uuid.UUID) ([]uuid.UUID, error) {
	lg := logger.Log().WithContext(ctx)
	query := fmt.Sprintf("SELECT %s FROM %s WHERE %s = ANY($1) AND %s = $2", columnName, tableName, columnName, condition)
	// Convert the IDs slice to a PostgreSQL array
	idsArray := pq.Array(ids)
	rows, err := trackRepo.db.QueryContext(ctx, query, idsArray, entityID)
	if err != nil {
		lg.Errorf("Database query error: %s", err.Error())
		return nil, err
	}
	defer rows.Close()

	var result []uuid.UUID
	foundIDs := make(map[string]bool) // To track which IDs were found

	// Initialize the foundIDs map
	for _, id := range ids {
		foundIDs[id.String()] = false
	}

	for rows.Next() {
		var id uuid.UUID
		if err := rows.Scan(&id); err != nil {
			lg.Errorf("Scanning error: %s", err.Error())
			return nil, err
		}
		result = append(result, id)
		// Mark the ID as found
		foundIDs[id.String()] = true
	}

	if err := rows.Err(); err != nil {
		lg.Errorf("Iteration error: %s", err.Error())
		return nil, err
	}

	// Check if any ID was not found
	for _, id := range ids {
		if !foundIDs[id.String()] {
			errMsg := fmt.Errorf("%s %s not found for the entity", columnName, id)
			lg.Errorf("%s: %s", columnName, errMsg.Error())
			return nil, consts.ErrNotFound
		}
	}

	return result, nil
}

// VerifyArtists verifies the existence of artists associated with a member in the database.
func (trackRepo *TrackRepo) VerifyArtists(ctx context.Context, memberID string, artistIDs ...uuid.UUID) ([]uuid.UUID, error) {
	return trackRepo.VerifyIDs(ctx, consts.TableArtist, "id", "member_id", memberID, artistIDs...)
}

// VerifyTrackGenres verifies the existence of genres associated with a partner in the database.
func (trackRepo *TrackRepo) VerifyTrackGenres(ctx context.Context, partnerID string, genreIDs ...uuid.UUID) ([]uuid.UUID, error) {
	return trackRepo.VerifyIDs(ctx, consts.TablePartnerGenres, "genre_id", "partner_id", partnerID, genreIDs...)
}

// VerifyRoles verifies the existence of roles associated with a partner in the database.
func (trackRepo *TrackRepo) VerifyRoles(ctx context.Context, partnerID string, roleIDs ...uuid.UUID) ([]uuid.UUID, error) {
	return trackRepo.VerifyIDs(ctx, consts.TablePartnerArtistRole, "role_id", "partner_id", partnerID, roleIDs...)
}

// UpdateTrackInfo updates the track information in the database.
func (trackRepo *TrackRepo) UpdateTrackInfo(ctx context.Context, params entities.UpdateTrackParams, trackID string) (int64, error) {

	lg := logger.Log().WithContext(ctx)
	updateTrack := `
		UPDATE public.track
		SET
			name = COALESCE($1, name),
			language_code = COALESCE($2, language_code),
			explicit = COALESCE($3, explicit),
			album_only = COALESCE($4, album_only),
			iswc = COALESCE($5, iswc),
			isrc = COALESCE($6, isrc),
			lyrics = COALESCE($7, lyrics),
			preview_start_at = COALESCE($8, preview_start_at),
			preview_duration = COALESCE($9, preview_duration),
			is_instrumental = COALESCE($10, is_instrumental),
			allow_download = COALESCE($11, allow_download),
			updated_on = $12

		WHERE id = $13
	`

	res, err := trackRepo.db.ExecContext(ctx, updateTrack,
		params.Name, params.LanguageCode, params.Explicit,
		params.AlbumOnly, params.ISWC, params.ISRC, params.Lyrics,
		params.PreviewStartAt, params.PreviewDuration,
		params.IsInstrumental, params.AllowDownload, time.Now(),
		trackID,
	)
	if err != nil {
		lg.Errorf("UpdateTrackInfo: Database update error: %s", err.Error())
		return 0, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		lg.Errorf("UpdateTrackInfo: Rows affected error: %s", err.Error())
		return 0, err
	}
	return rowsAffected, nil
}

// UpdateTrackTx updates the track information within a transaction.
func (trackRepo *TrackRepo) UpdateTrackTx(ctx context.Context, arg entities.TrackProperties) (int64, error) {

	lg := logger.Log().WithContext(ctx)
	trackID := arg.GetTrackID()
	var rowsAffected int64
	err := trackRepo.ExecuteTx(ctx, func(tx *sql.Tx) error {
		var err error
		// Updates info in the track table
		rowsAffected, err = trackRepo.UpdateTrackInfo(ctx, entities.UpdateTrackParams{
			Name: sql.NullString{
				String: arg.GetTrackName(),
				Valid:  arg.KeyExists("name"),
			},
			LanguageCode: sql.NullString{
				String: arg.GetLangCode(),
				Valid:  arg.KeyExists("language"),
			},
			Explicit: sql.NullInt16{
				Int16: arg.GetExplicit(),
				Valid: arg.KeyExists("explicit"),
			},
			AlbumOnly: sql.NullBool{
				Bool:  arg.GetAlbumOnly(),
				Valid: arg.KeyExists("album_only"),
			},
			ISWC: sql.NullString{
				String: arg.GetISWC(),
				Valid:  arg.KeyExists("iswc"),
			},
			PreviewStartAt: sql.NullInt32{
				Int32: arg.GetPreviewStartAt(),
				Valid: arg.KeyExists("preview_start_at"),
			},
			PreviewDuration: sql.NullInt32{
				Int32: arg.GetPreviewDuration(),
				Valid: arg.KeyExists("preview_duration"),
			},
			IsInstrumental: sql.NullBool{
				Bool:  arg.GetIsInstrumental(),
				Valid: arg.KeyExists("is_instrumental"),
			},
			AllowDownload: sql.NullBool{
				Bool:  arg.GetAllowDownload(),
				Valid: arg.KeyExists("allow_download"),
			},
			Lyrics: sql.NullString{
				String: arg.GetLyrics(),
				Valid:  arg.KeyExists("lyrics"),
			},
			ISRC: sql.NullString{
				String: arg.GetISRC(),
				Valid:  arg.KeyExists("isrc"),
			},
		}, arg.GetTrackID())
		if err != nil {
			// Log the error if updating the track info fails.
			lg.Errorf("UpdateTrackTx: Error updating track info: %s", err.Error())
			return err
		}

		// If true, delete the existing values
		if artists, featuredArtist, err := arg.GetArtists(); artists != nil && err == nil {
			err := trackRepo.DeleteTrackProperties(ctx, trackID, consts.TableTrackArtist)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error deleting track artists: %s", err.Error())
				return err
			}
			err = trackRepo.InsertTrackArtists(ctx, trackID, artists...)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error inserting track artists: %s", err.Error())
				return err
			}
			err = trackRepo.InsertFeaturedArtists(ctx, trackID, featuredArtist...)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error inserting track featured artists: %s", err.Error())
				return err
			}
		}

		// If true, delete the existing values
		// if arg.KeyExists(consts.FeaturedArtists) {
		// 	if featuredArtists, err := arg.GetFeaturedArtists(); err == nil {
		// 		err = trackRepo.DeleteTrackProperties(ctx, trackID, consts.TableFeaturedArtist)
		// 		if err != nil {
		// 			lg.Errorf("UpdateTrackTx: Error deleting featured artists: %s", err.Error())
		// 			return err
		// 		}
		// 		// Updates featured artists
		// 		if featuredArtists != nil {
		// 			err = trackRepo.InsertFeaturedArtists(ctx, trackID, featuredArtists...)
		// 			if err != nil {
		// 				lg.Errorf("UpdateTrackTx: Error inserting featured artists: %s", err.Error())
		// 				return err
		// 			}
		// 		}
		// 	}

		// }

		// If true, delete the existing values
		if genres, err := arg.GetGenres(); genres != nil && err == nil {
			err = trackRepo.DeleteTrackProperties(ctx, trackID, consts.TableTrackGenre)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error deleting track genres: %s", err.Error())
				return err
			}
			// Updates track genres
			err = trackRepo.InsertTrackGenres(ctx, trackID, genres...)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error inserting track genres: %s", err.Error())
				return err
			}
		}

		// If true, delete the existing values
		if participants, err := arg.GetParticipants(); participants != nil && err == nil {
			err := trackRepo.DeleteTrackProperties(ctx, trackID, consts.TableTrackArtistRole)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error deleting track participants: %s", err.Error())
				return err
			}
			// Updates track participants
			err = trackRepo.InsertTrackParticipants(ctx, trackID, participants...)
			if err != nil {
				lg.Errorf("UpdateTrackTx: Error inserting track participants: %s", err.Error())
				return err
			}
		}

		return nil
	})

	return rowsAffected, err
}

// GetID retrieves an ID from the specified table and field in the database.
func (trackRepo *TrackRepo) GetID(ctx context.Context, tableName, field, fieldValue, returnField string) (any, error) {

	lg := logger.Log().WithContext(ctx)

	var result interface{}
	query := `SELECT ` + returnField + ` FROM ` + tableName + `
	WHERE ` + field + ` = $1 `
	row := trackRepo.db.QueryRowContext(ctx, query, fieldValue)
	err := row.Scan(&result)
	if err != nil {
		lg.Errorf("GetId: Error retrieving ID: %s", err.Error())
		return nil, err
	}
	return result, nil
}

// UpdateTrackMetadata updates the metadata of a track in the database.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@metadata: A TrackMetadata struct containing the updated metadata for the track.
//
// Behavior:
//   - Executes a SQL update query to modify the metadata fields of the track with the provided track ID.
//
// Returns:
//   - An error if the database update operation fails.
//   - Nil error if the update is successful.
//
// Note:
//   - This function is typically used to update information about a track, such as its filename, duration, status, and more.
//
// Example:
//   - err := UpdateTrackMetadata(ctx, metadata)
//   - if err != nil {
//     // Handle the error
//     }
func (repo *TrackRepo) UpdateTrackMetadata(ctx context.Context, metadata entities.TrackMetadata) error {
	query := `
		UPDATE track
		SET 
			filename = $1,
			duration = $2, 
			status = $3, 
			file_size = $4, 
			file_ext = $5, 
			sample_rate = $6,
			updated_on = $7
		WHERE id = $8
	`
	_, err := repo.db.ExecContext(ctx,
		query,
		metadata.FileName,
		metadata.Duration,
		metadata.Status,
		metadata.FileSize,
		metadata.FileExt,
		metadata.SampleRate,
		metadata.UpdatedOn,
		metadata.ID,
	)

	if err != nil {
		return err
	}

	return nil
}

// ValidateTrackOwner checks if a given track is owned by a specific member.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to validate ownership for.
//	@memberID: The ID of the member to validate ownership against.
//
// Behavior:
//   - Counts the number of tracks associated with the specified member and track ID.
//   - Returns true if the member owns the track (count is greater than zero), otherwise false.
//
// Returns:
//   - A boolean indicating whether the track is owned by the member.
//   - An error if there is an issue with the database query.
//
// Note:
//   - This function is used to determine if a member has ownership rights to a particular track.
//
// Example:
//   - isOwner, err := ValidateTrackOwner(ctx, trackID, memberID)
//   - if err != nil {
//     // Handle the error
//     }
//   - if isOwner {
//     // The member owns the track
//     } else {
//     // The member does not own the track
//     }
func (repo *TrackRepo) ValidateTrackOwner(ctx context.Context, trackID, memberID string) (bool, error) {

	count, err := repo.CountTrackMember(ctx, trackID, memberID)

	if err != nil {
		return false, err
	}
	if count == 0 {
		return false, nil
	}
	return true, nil
}

// CountTrackMember returns the count of tracks associated with a specific member.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to validate ownership for.
//	@memberID: The ID of the member to count tracks for.
//
// Behavior:
//   - Executes a SQL query to count the number of tracks in the database that are associated with both the specified member and track ID.
//
// Returns:
//   - The count of tracks associated with the member and track.
//   - An error if there is an issue with the database query.
//
// Note:
//   - This function can be used to check how many tracks are associated with a specific member.
//
// Example:
//   - count, err := CountTrackMember(ctx, trackID, memberID)
//   - if err != nil {
//     // Handle the error
//     }
//   - // Use the count to determine the number of tracks associated with the member.
func (repo *TrackRepo) CountTrackMember(ctx context.Context, trackID, memberID string) (int, error) {

	query := "SELECT COUNT(*) FROM track WHERE member_id=$1 AND id=$2"
	var count int

	err := repo.db.QueryRowContext(ctx, query, memberID, trackID).Scan(&count)

	if err != nil {
		return 0, err
	}

	return count, nil
}

// GetTrack retrieves various track-related information by combining data from different methods.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to retrieve information for.
//
// Behavior:
//   - Calls multiple methods to retrieve various details about a track.
//   - Combines the data from these methods to create a comprehensive representation of the track.
//
// Returns:
//   - A Track struct containing detailed information about the track.
//   - An error if there are issues with any of the data retrieval methods.
//   - Nil error if the track data is successfully retrieved.
//
// Note:
//   - This function serves as a higher-level method that retrieves and combines different aspects of a track's information.
//
// Example:
//   - trackData, err := GetTrack(ctx, trackID)
//   - if err != nil {
//     // Handle the error
//     }
//   - // Use trackData to access different aspects of the track's information.
func (track *TrackRepo) GetTrack(ctx context.Context, trackID string) (entities.Track, error) {
	var trackData entities.Track
	var err error
	trackData, err = track.GetTrackData(ctx, trackID)
	if err != nil {
		return trackData, err
	}

	trackData.FeaturedArtists, err = track.GetFeaturedArtist(ctx, trackID)
	if err != nil {
		return trackData, err
	}

	trackData.Participants, err = track.GetParticipants(ctx, trackID)
	if err != nil {
		return trackData, err
	}

	trackData.Genre, err = track.GetGenre(ctx, trackID)
	if err != nil {
		return trackData, err
	}

	trackData.Artists, err = track.GetArtists(ctx, trackID)
	if err != nil {
		return trackData, err
	}
	return trackData, nil
}

// GetTrackData retrieves basic track information by querying the database.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to retrieve information for.
//
// Behavior:
//   - Executes a SQL query to retrieve basic information about the track, such as its name, explicit status, language, etc.
//
// Returns:
//   - A Track struct containing the basic information about the track.
//   - An error if there is an issue with the database query.
//   - Nil error if the basic track data is successfully retrieved.
//
// Example:
//   - trackData, err := GetTrackData(ctx, trackID)
//   - if err != nil {
//     // Handle the error
//     }
//   - // Use trackData to access basic track information.
func (track *TrackRepo) GetTrackData(ctx context.Context, trackID string) (entities.Track, error) {

	var tracks entities.Track
	var status int
	err := track.db.QueryRowContext(ctx, `
		SELECT 
			track.id, 
			track.name, 
			track.explicit, 
			track.album_only, 
			COALESCE(track.iswc, '') 
			AS iswc, track.isrc, 
			COALESCE(track.lyrics, '') AS lyrics, 
			track.allow_download,
			track.status, 
			language.id, 
			language.name, 
			language.code
		FROM public.track
		JOIN public.language
		ON track.language_code = language.code 
		AND track.id=$1`, trackID).Scan(
		&tracks.ID,
		&tracks.Name,
		&tracks.Explicit,
		&tracks.AlbumOnly,
		&tracks.ISWC,
		&tracks.ISRC,
		&tracks.Lyrics,
		&tracks.AllowDownload,
		&status,
		&tracks.Languages.ID,
		&tracks.Languages.Name,
		&tracks.Languages.Code)

	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetTrackData failed , QueryRowContext, Error = %s", err.Error())
		return tracks, err
	}
	tracks.Status = utilities.StatusToString(status)
	return tracks, nil
}

// GetFeaturedArtist retrieves a list of featured artists for a given track.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to retrieve featured artists for.
//
// Behavior:
//   - Executes a SQL query to retrieve a list of featured artists for the track.
//
// Returns:
//   - A slice of FeaturedArtist structs representing the featured artists.
//   - An error if there is an issue with the database query.
//   - Nil error if the featured artist data is successfully retrieved.
//
// Example:
//   - featuredArtists, err := GetFeaturedArtist(ctx, trackID)
//   - if err != nil {
//     // Handle the error
//     }
//   - // Use featuredArtists to access details about featured artists on the track.
func (track *TrackRepo) GetFeaturedArtist(ctx context.Context, trackID string) ([]entities.FeaturedArtist, error) {
	var featuredArtists []entities.FeaturedArtist

	query := `
		SELECT 
			artist.id, 
			artist.name 
		FROM public.artist
		JOIN public.track_featured_artist
		ON track_featured_artist.artist_id = artist.id AND track_featured_artist.track_id = $1;`
	rows, err := track.db.QueryContext(ctx, query, trackID)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetFeaturedArtist failed , QueryContext, Error = %s", err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var featuredArtistInfo entities.FeaturedArtist
		err := rows.Scan(&featuredArtistInfo.ID, &featuredArtistInfo.Name)

		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetFeaturedArtist failed , rows scan function ,Error = %s", err.Error())
			return nil, err
		}

		featuredArtists = append(featuredArtists, featuredArtistInfo)
	}

	if err := rows.Err(); err != nil {
		logger.Log().WithContext(ctx).Errorf("GetFeaturedArtist failed , Error = %s", err.Error())
		return nil, err
	}

	return featuredArtists, nil
}

// GetGenre retrieves a list of genres for a given track.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to retrieve genres for.
//
// Behavior:
//   - Executes a SQL query to retrieve a list of genres associated with the track.
//
// Returns:
//   - A slice of Genre structs representing the genres associated with the track.
//   - An error if there is an issue with the database query.
//   - Nil error if the genre data is successfully retrieved.
//
// Example:
//   - genres, err := GetGenre(ctx, trackID)
//   - if err != nil {
//     // Handle the error
//     }
//   - // Use genres to access details about genres associated with the track.
func (track *TrackRepo) GetGenre(ctx context.Context, trackID string) ([]entities.Genre, error) {
	var genre []entities.Genre

	query := `
		SELECT 
			genre.id, 
			genre.name
		FROM public.track_genre
		JOIN public.genre
		on track_genre.genre_id = genre.id AND track_genre.track_id = $1`
	rows, err := track.db.QueryContext(ctx, query, trackID)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetGenre failed , QueryContext, Error = %s", err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var genreInfo entities.Genre
		err := rows.Scan(&genreInfo.ID, &genreInfo.Name)

		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetGenre failed , rows scan function ,Error = %s", err.Error())
			return nil, err
		}

		genre = append(genre, genreInfo)
	}

	if err := rows.Err(); err != nil {
		logger.Log().WithContext(ctx).Errorf("GetGenre failed , Error = %s", err.Error())
		return nil, err
	}

	return genre, nil
}

// GetParticipants retrieves a list of participant artists and their roles for a given track.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to retrieve participants for.
//
// Behavior:
//   - Executes a SQL query to retrieve a list of participant artists and their roles for the track.
//
// Returns:
//   - A slice of Participant structs representing the participants and their roles on the track.
//   - An error if there is an issue with the database query.
//   - Nil error if the participant data is successfully retrieved.
//
// Example:
//   - participants, err := GetParticipants(ctx, trackID)
//   - if err is not nil {
//     // Handle the error
//     }
//   - // Use participants to access details about artists and their roles on the track.
func (track *TrackRepo) GetParticipants(ctx context.Context, trackID string) ([]entities.Participant, error) {
	var participants []entities.Participant

	query := `
		SELECT 
			artist.id, 
			artist.name, 
			role.id,
			role.name, 
			percentage 
		FROM public.track_artist_role
		JOIN public.artist 
		ON artist.id=track_artist_role.artist_id
		JOIN public.role
		ON track_artist_role.role_id=role.id
		WHERE track_artist_role.track_id=$1`
	rows, err := track.db.QueryContext(ctx, query, trackID)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetParticipants failed , QueryContext, Error = %s", err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var participantsInfo entities.Participant
		err := rows.Scan(&participantsInfo.ID, &participantsInfo.Name, &participantsInfo.RoleID, &participantsInfo.Role, &participantsInfo.Percentage)

		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetParticipants failed , rows scan function ,Error = %s", err.Error())
			return nil, err
		}

		participants = append(participants, participantsInfo)
	}

	if err := rows.Err(); err != nil {
		logger.Log().WithContext(ctx).Errorf("GetParticipants failed , Error = %s", err.Error())
		return nil, err
	}

	return participants, nil
}

// GetArtists retrieves a list of artists for a given track.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@trackID: The ID of the track to retrieve artists for.
//
// Behavior:
//   - Executes a SQL query to retrieve a list of artists associated with the track.
//
// Returns:
//   - A slice of Artist structs representing the artists associated with the track.
//   - An error if there is an issue with the database query.
//   - Nil error if the artist data is successfully retrieved.
//
// Example:
//   - artists, err := GetArtists(ctx, trackID)
//   - if err is not nil {
//     // Handle the error
//     }
//   - // Use artists to access details about artists associated with the track.
func (track *TrackRepo) GetArtists(ctx context.Context, trackID string) ([]entities.Artist, error) {
	var artists []entities.Artist

	query := `
		SELECT 
			artist.id, 
			artist.name 
		FROM public.track_artist
		JOIN public.artist 
		ON artist.id=track_artist.artist_id
		AND track_artist.track_id=$1;`
	rows, err := track.db.QueryContext(ctx, query, trackID)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetArtists failed , QueryContext, Error = %s", err.Error())
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var artistsInfo entities.Artist
		err := rows.Scan(&artistsInfo.ID, &artistsInfo.Name)

		if err != nil {
			logger.Log().WithContext(ctx).Errorf("GetArtists failed , rows scan function ,Error = %s", err.Error())
			return nil, err
		}

		artists = append(artists, artistsInfo)
	}

	if err := rows.Err(); err != nil {
		logger.Log().WithContext(ctx).Errorf("GetArtists failed , Error = %s", err.Error())
		return nil, err
	}

	return artists, nil
}

func (repo *TrackRepo) UpdateConvertionStatus(ctx context.Context, userID string, trackID string, reqbody entities.UpdateTrackStatusRequest) error {
	_, err := repo.db.ExecContext(ctx, "UPDATE track SET status = $1, sample_rate = $2, file_ext = $3, file_size = $4 WHERE member_id = $5 AND id = $6", reqbody.Status, reqbody.SampleRate, reqbody.FileExtension, reqbody.FileSize, userID, reqbody.TrackID)
	log := logger.Log().WithContext(ctx)
	if err != nil {
		if ctx.Err() == context.Canceled {
			// Handle context cancellation error
			log.Errorf("Repository: Context canceled during database operation, %s", err.Error())
			return ctx.Err()
		}
		log.Errorf("Repository: Failed to update track status: %v", err)
		return err
	}
	return nil
}

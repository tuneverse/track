package controllers

import (
	"errors"
	"net/http"
	"strings"
	"track/internal/consts"
	"track/internal/entities"
	"track/internal/usecases"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	constants "gitlab.com/tuneverse/toolkit/consts"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// TrackController represents a controller responsible for handling Track-related API requests.
type TrackController struct {
	router   *gin.RouterGroup
	useCases usecases.TrackUseCaseImply
}

// NewTrackController creates a new TrackController instance.
func NewTrackController(router *gin.RouterGroup, TrackUseCase usecases.TrackUseCaseImply) *TrackController {
	return &TrackController{
		router:   router,
		useCases: TrackUseCase,
	}
}

// InitRoutes initializes and configures the Track-related routes for the TrackController.
func (track *TrackController) InitRoutes() {
	track.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "HealthHandler")
	})
	track.router.DELETE("/:version/tracks/:track_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "DeleteTrack")
	})
	track.router.PATCH("/:version/tracks/:track_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "UpdateTrackMetadata")
	})

	track.router.GET("/:version/tracks/:track_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "GetTrack")
	})

	track.router.POST("/:version/tracks", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "UploadTrack")
	})

	track.router.PUT("/:version/tracks/:track_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "UpdateTrack")
	})

	track.router.PATCH("/:version/tracks/:track_id/update-status", func(ctx *gin.Context) {
		version.RenderHandler(ctx, track, "UpdateConvertionStatus")
	})

}

// UploadTrack handles the uploading of a track file to the system.
// It extracts the necessary information from the request context and form data
// and triggers the appropriate use case to perform the upload.
//
// Parameters:
//
//	@ctx: The Gin context for the HTTP request.
//
// Behavior:
//   - Extracts the member ID and partner ID from the Gin context.
//   - Retrieves the uploaded file from the form data.
//   - Validates the request and file data.
//   - Initiates the track upload operation using use cases.
//
// Returns:
//   - JSON response with a success message and HTTP status 200 if the upload is successful.
//   - JSON response with an error message and HTTP status 400 if there are issues with the request or file.
//   - JSON response with an error message and HTTP status 500 if there are validation or internal server errors.
func (track *TrackController) UploadTrack(ctx *gin.Context) {
	userID := ctx.MustGet("member_id").(string)
	PartnerID := ctx.MustGet("partner_id").(string)

	uploadedfile, fileheader, err := ctx.Request.FormFile("file")
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": "failed to retrieve the file from form data",
		})
		return
	}
	contextError, _ := utils.GetContext[map[string]any](ctx, constants.ContextErrorResponses)
	ctxt := ctx.Request.Context()
	method := strings.ToLower(ctx.Request.Method)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)
	log := logger.Log().WithContext(ctxt)

	if !isEndpointExists {
		log.Errorf("Failed to upload track: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   constants.EndpointErr,
			"errors":    nil,
		})
		return
	}

	fieldMap, err := track.useCases.UploadTrack(ctxt, userID, PartnerID, uploadedfile, fileheader)
	if len(fieldMap) != 0 {
		log.Errorf("Track upload failed, validation error")
		fields := utils.FieldMapping(fieldMap)
		val, _, errorCode := utils.ParseFields(ctx, constants.ValidationErr,
			fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}
	if err != nil {
		// for logging error message
		log.Errorf("Track upload failed, validation error")
		// Retrieve error based on the api's requirement from the errors which are stored in context######
		val, _, errorCode := utils.ParseFields(ctx, constants.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	//This is just an example. Use this wisely.
	log.Errorf("TrackUpload: completed")

	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "track uploaded successfully",
	})
}

// DeleteTrack handles the deletion of a track based on the provided context in a Gin request.
//
// Parameters:
//
//	@ctx: The Gin context containing request data.
//
// Functionality:
//   - Extracts user, partner, and track identifiers from the Gin context.
//   - Retrieves the context-specific error responses and endpoints.
//   - Logs errors and performs validation checks.
//   - Calls the DeleteTrack function of the useCases to delete the track.
//   - Handles and responds to different scenarios with appropriate status codes and messages.
//
// Detailed Steps:
//  1. Extract user, partner, and track identifiers from the Gin context.
//  2. Retrieve context-specific error responses and endpoints.
//  3. Log errors and validate the request, checking for the existence of the endpoint.
//  4. If the endpoint is invalid, log an error and respond with a 400 Bad Request status.
//  5. If there are validation errors, log them, format and parse error fields, and respond with the appropriate status code and error messages.
//  6. If there are no validation errors, proceed to delete the track by calling the useCases' DeleteTrack function.
//  7. Handle any errors that occur during track deletion, logging errors and responding with appropriate status codes.
//  8. If track deletion is successful, log the completion and respond with a 200 OK status and success message.
func (track *TrackController) DeleteTrack(ctx *gin.Context) {
	userID := ctx.GetString("member_id")
	partnerID := ctx.GetString("partner_id")
	trackID := ctx.Param("track_id")

	contextError, _ := utils.GetContext[map[string]any](ctx, constants.ContextErrorResponses)
	ctxt := ctx.Request.Context()
	method := strings.ToLower(ctx.Request.Method)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)
	log := logger.Log().WithContext(ctxt)

	if !isEndpointExists {
		log.Errorf("Failed to delete track: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   constants.EndpointErr,
			"errors":    nil,
		})
		return
	}
	fieldMap, err := track.useCases.DeleteTrack(ctxt, trackID, userID, partnerID)
	if len(fieldMap) != 0 {
		log.Errorf("Track upload failed, validation error")
		fields := utils.FieldMapping(fieldMap)
		val, _, errorCode := utils.ParseFields(ctx, constants.ValidationErr,
			fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}
	if err != nil {
		val, _, errorCode := utils.ParseFields(ctx, constants.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	log.Errorf("TrackDelete: completed")

	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "track deleted successfully",
	})
}

// UpdateTrackMetadata handles the update of metadata for a track.
// It receives the new track properties from the request and triggers the
// appropriate use case to perform the update.
//
// Parameters:
//
//	@ctx: The Gin context for the HTTP request.
//
// Behavior:
//   - Parses and validates the incoming track properties from the request body.
//   - Retrieves member ID, partner ID, and track ID from the Gin context.
//   - Validates the request and endpoint information.
//   - Calls the use case to update the track metadata.
//   - Handles different error scenarios and returns appropriate JSON responses.
//
// Returns:
//   - JSON response with a success message and HTTP status 200 if the update is successful.
//   - JSON response with an error message and HTTP status 400 if there are issues with the request or data.
//   - JSON response with an error message and HTTP status 404 if the track is not found.
//   - JSON response with an error message and HTTP status 500 if there are validation or internal server errors.
func (trackCtrl *TrackController) UpdateTrackMetadata(ctx *gin.Context) {

	var (
		trackInfo entities.TrackProperties
		ctxt      = ctx.Request.Context()
		lg        = logger.Log().WithContext(ctxt)
	)

	if err := ctx.Bind(&trackInfo); err != nil {
		lg.Errorf("UpdateTrackMetadata failed, invalid endpoint, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, models.ErrorResponse{
			ErrorCode: http.StatusBadRequest,
			Message:   err.Error(),
		})
		return
	}
	partnerID, memberID := ctx.GetString("partner_id"), ctx.GetString("member_id")
	httpMethod, endpointURL := strings.ToLower(ctx.Request.Method), ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, httpMethod)

	if !isEndpointExists {
		lg.Error("UpdateTrackMetadata failed, invalid endpoint")
		ctx.JSON(http.StatusBadRequest, models.ErrorResponse{
			ErrorCode: http.StatusBadRequest,
			Message:   constants.EndpointErr,
			Errors:    nil,
		})
		return
	}

	trackID := ctx.Param("track_id")
	trackInfo["id"] = trackID
	trackInfo["member_id"] = memberID

	contextError, _ := utils.GetContext[map[string]any](ctx, constants.ContextErrorResponses)
	validationErr, err := trackCtrl.useCases.UpdateTrackMetadata(ctx, trackInfo, partnerID)
	if err != nil {
		switch {
		case errors.Is(err, consts.ErrInvalidInput):
			lg.Errorf("UpdateTrackMetadata: invalid input params err=%s", err.Error())
			val, _, errorCode := utils.ParseFields(ctx, constants.ValidationErr,
				consts.InvalidInput, contextError, endpoint, httpMethod)
			ctx.JSON(int(errorCode), val)
			return
		case errors.Is(err, consts.ErrNotFound):
			lg.Errorf("UpdateTrackMetadata track not found err=%s", err.Error())
			val, _, errorCode := utils.ParseFields(ctx, consts.NotFound,
				"", contextError, "", "")
			ctx.JSON(int(errorCode), val)
			return
		default:
			lg.Errorf("UpdateTrackMetadata failed err=%s", err.Error())
			val, _, errorCode := utils.ParseFields(ctx, constants.InternalServerErr,
				"", contextError, "", "")
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	if len(validationErr) != 0 {
		lg.Error("UpdateTrackMetadata failed, Validation errors")
		fields := utils.FieldMapping(validationErr)
		val, _, errorCode := utils.ParseFields(ctx, constants.ValidationErr,
			fields, contextError, endpoint, httpMethod)
		ctx.JSON(int(errorCode), val)
		return
	}

	resp := entities.Response{
		StatusCode: http.StatusOK,
		Message:    "Track metadata updated successfully.",
		Data:       nil,
	}
	ctx.JSON(http.StatusOK, resp)
}

// UpdateTrack handles the update of a track, including the uploaded file and associated metadata.
//
// Parameters:
//
//	@ctx: The Gin context for the HTTP request.
//
// Behavior:
//   - Extracts member ID and partner ID from the Gin context.
//   - Checks if member ID or partner ID is empty and returns an error if so.
//   - Retrieves the uploaded file and related file header from the HTTP request form data.
//   - Handles errors related to file retrieval and returns appropriate JSON responses.
//   - Extracts context information and performs endpoint validation.
//   - Checks the validity of the track ID by parsing it as a UUID.
//   - Calls the use case to update the track.
//   - Handles different error scenarios and returns appropriate JSON responses.
//
// Returns:
//   - JSON response with a success message and HTTP status 200 if the update is successful.
//   - JSON response with an error message and HTTP status 400 if there are issues with the request or data.
//   - JSON response with an error message and HTTP status 404 if the track is not found.
//   - JSON response with an error message and HTTP status 500 if there are validation or internal server errors.
func (track *TrackController) UpdateTrack(ctx *gin.Context) {

	memberID := ctx.MustGet("member_id").(string)
	partnerID := ctx.MustGet("partner_id").(string)

	// Check if memberID or partnerID is empty.
	if memberID == "" || partnerID == "" {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Context values not found or empty"})
		return
	}

	// Retrieve the uploaded file
	uploadedfile, fileheader, err := ctx.Request.FormFile("file")
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": "failed to retrieve the file from form data",
		})
		return
	}

	contextError, _ := utils.GetContext[map[string]any](ctx, constants.ContextErrorResponses)

	// Extract the context from the request.
	ctxt := ctx.Request.Context()

	log := logger.Log().WithContext(ctxt)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)

	// Get the endpoint URL from the context
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	if !isEndpointExists {

		log.Errorf("Failed to upload track: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   constants.EndpointErr,
			"errors":    nil,
		})
		return
	}

	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	// Get the track ID from the request parameters
	trackID := ctx.Param("track_id")

	// Check if the track ID is a valid UUID
	_, err = uuid.Parse(trackID)
	if err != nil {

		log.Errorf("UpdateTrack failed, Trackid notfound, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.ResourceNotFound,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	fieldMap, err := track.useCases.UpdateTrack(ctxt, memberID, partnerID, uploadedfile, fileheader, trackID)

	if err != nil {

		log.Errorf("Track upload failed, validation error")
		// Retrieve error based on the api's requirement from the errors which are stored in context
		val, _, errorCode := utils.ParseFields(ctx, constants.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	if len(fieldMap) != 0 {

		log.Errorf("Track upload failed, validation error")
		fields := utils.FieldMapping(fieldMap)
		val, _, errorCode := utils.ParseFields(ctx, constants.ValidationErr,
			fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "track uploaded successfully",
	})
}

// GetTrack retrieves track data based on the provided track ID, member ID, and partner ID.
//
// Parameters:
//
//	@ctx: The Gin context for the HTTP request.
//
// Behavior:
//   - Extracts member ID and partner ID from the Gin context.
//   - Creates a Track struct and assigns member ID and partner ID to it.
//   - Extracts context information and performs endpoint validation.
//   - Checks the validity of the track ID by parsing it as a UUID.
//   - Calls the use case to fetch track data.
//   - Handles different error scenarios and returns appropriate JSON responses.
//
// Returns:
//   - JSON response with track data and HTTP status 200 if the data is successfully retrieved.
//   - JSON response with an error message and HTTP status 400 if there are issues with the request or data.
//   - JSON response with an error message and HTTP status 404 if the track is not found.
//   - JSON response with an error message and HTTP status 500 if there are validation or internal server errors.
func (track *TrackController) GetTrack(ctx *gin.Context) {

	// Todo Get memberId and partnerId from token

	memberID := ctx.MustGet("member_id").(string)
	partnerID := ctx.MustGet("partner_id").(string)

	//assign member id and partner id to struct
	var trackData entities.Track
	trackData.MemberID, trackData.PartnerID = memberID, partnerID

	// Extract the context from the request.
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)

	methods := ctx.Request.Method
	method := strings.ToLower(methods)

	// Get the endpoint URL from the context
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)

	if !isEndpointExists {

		log.Errorf("GetTrack failed: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   constants.EndpointErr,
			"errors":    nil,
		})
		return
	}
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	contextError, _ := utils.GetContext[map[string]any](ctx, constants.ContextErrorResponses)

	// Get the track ID from the request parameters
	trackID := ctx.Param("track_id")

	// Check if the track ID is a valid UUID
	_, err := uuid.Parse(trackID)
	if err != nil {

		log.Errorf("GetTrack failed, Trackid notfound, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.ResourceNotFound,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	trackData.ID = trackID

	// Call the useCases.GetTrack function to fetch track data
	fieldsMap, trackData, err := track.useCases.GetTrack(ctx, trackData)

	//return internal server error
	if err != nil {

		log.Errorf("GetTrack failed, internal server error, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, constants.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	if len(fieldsMap) != 0 {

		fields := utils.FieldMapping(fieldsMap)
		log.Errorf("GetTrack failed, validation error, err = %s	", fields)
		val, _, errorCode := utils.ParseFields(ctx, constants.ValidationErr,
			fields, contextError, endpoint, method)

		ctx.JSON(int(errorCode), val)
		return
	}

	log.Info("Track data fetched successfully.")

	// Return a success response with the retrieved track data
	ctx.JSON(http.StatusOK, entities.SuccessResponse{
		Code:    200,
		Message: "track data retrieved successfully",
		Data:    trackData,
	})

}

// UpdateConvertionStatus handles updating the status of a track.
// It extracts the necessary information from the request context and triggers the
// appropriate use case to perform the status update.
//
// Parameters:
//
//	@ctx: The Gin context for the HTTP request.
//
// Behavior:
//   - Extracts the user ID, partner ID, and track ID from the Gin context.
//   - Retrieves the status from the request body.
//   - Calls the use case to update the track status.
//   - Responds with JSON based on the success or failure of the operation.
func (track *TrackController) UpdateConvertionStatus(ctx *gin.Context) {
	var reqBody entities.UpdateTrackStatusRequest

	// Parse the JSON request body into the struct
	if err := ctx.BindJSON(&reqBody); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userID := ctx.GetString("member_id")
	trackID := ctx.Param("track_id")

	// Call the use case to update the track status
	err := track.useCases.UpdateConvertionStatus(ctx.Request.Context(), userID, trackID, reqBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "failed to update track status",
		})
		return
	}

	// Respond with success
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "track status updated successfully",
	})
}

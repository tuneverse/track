package usecases

import (
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"mime/multipart"
	"os"
	"testing"
	"time"

	"track/internal/cloud/awsutils/mockcloud"
	"track/internal/consts"
	"track/internal/entities"
	"track/internal/repo/mock"
	"track/internal/repo/mockqueue"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/utils/audio"
)

type CustomFile struct {
	*bytes.Reader
	size int64
}

// NewCustomFile creates a new CustomFile from a byte slice.
func NewCustomFile(data []byte) *CustomFile {
	return &CustomFile{Reader: bytes.NewReader(data), size: int64(len(data))}
}

// TestMain is the entry point for running tests in this test suite.
// It configures the testing environment, initializes the logger, and executes the test suite.
// The function sets Gin web framework to test mode, configures the logger with specified options,
// and then runs all the test functions. The exit code reflects the success or failure of the tests.
func TestMain(t *testing.M) {
	// Set Gin to test mode for optimized testing behavior.
	gin.SetMode(gin.TestMode)

	// Configure logger options, including service name, log level, and data inclusion in logs.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}

	// Initialize the logger with the specified options.
	_ = logger.InitLogger(clientOpt)

	// Run all test functions and exit with an appropriate status code.
	os.Exit(t.Run())
}

func TestDeleteTrack(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create mock objects
	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	// Ensure that mockRepo and mockCloud are not nil
	if mockRepo == nil {
		t.Fatal("mockRepo is nil")
	}
	if mockCloud == nil {
		t.Fatal("mockCloud is nil")
	}

	mockRepo.EXPECT().ValidateTrackOwnership(gomock.Any(), "track123", "member456").Return(nil)
	mockRepo.EXPECT().CheckTrackIsAssignedToProduct(gomock.Any(), "track123").Return(nil)
	mockRepo.EXPECT().GetFileExtension(gomock.Any(), "track123", "member456").Return("flac", nil)
	mockRepo.EXPECT().DeleteTrack(gomock.Any(), gomock.Any()).Return(nil)
	mockCloud.EXPECT().DeleteObject(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
	mockRepo.EXPECT().GetISRCByTrackID(gomock.Any(), "track123").Return("INXYZ2300033", nil) // Replace "your_isrc_value" with the expected ISRC value.
	mockRepo.EXPECT().UpdateIsrcStatus(gomock.Any(), "INXYZ2300033").Return(nil)             // Replace "your_isrc_value" with the expected ISRC value.
	// Create TrackUseCases with mock objects
	track := &TrackUseCases{
		repo:  mockRepo,
		cloud: mockCloud,
		env: &entities.EnvConfig{
			AWS: entities.AWS{
				BucketName: "tuneversev2",
			},
		},
	}

	// Call the DeleteTrack function
	ctx := context.TODO()
	trackId := "track123"
	memberId := "member456"
	partnerId := "partner789"

	result, err := track.DeleteTrack(ctx, trackId, memberId, partnerId)

	if err != nil {
		t.Errorf("DeleteTrack failed: %v", err)
	}

	if result == nil {
		t.Error("DeleteTrack result is nil")
	}
}

func TestUploadTrack(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	trackUseCases := &TrackUseCases{
		repo:  mockRepo,
		cloud: mockCloud,
	}

	ctx := context.Background()
	userID := "testUserID"
	partnerID := "testPartnerID"
	file := &multipart.FileHeader{
		Filename: "audio.mp3",
		Size:     1024,
	}

	fileContent := []byte("This is the content of the file")

	dummyFile := NewCustomFile(fileContent)
	dummyFileHeader := &multipart.FileHeader{
		Filename: "audio.mp3",
		Size:     dummyFile.Size(),
	}

	t.Run("SuccessfulUpload", func(t *testing.T) {
		// Execute your code
		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("UnsupportedFileExtension", func(t *testing.T) {
		file.Filename = "audio.unsupported"
		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)

	})

	t.Run("FileSizeExceedsMaximum", func(t *testing.T) {
		file.Size = int64(consts.MaxFileSizeMb * 1024 * 1024 * 2) // Set file size to exceed the maximum
		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)

	})

	t.Run("EmptyFileName", func(t *testing.T) {
		file.Filename = ""
		userID := ""
		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)

	})

	t.Run("ValidUploadWithSupportedExtensionAndSize", func(t *testing.T) {
		// Create a file with valid size and supported extension

		fileContent := []byte("This is a valid MP3 file")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("ValidUploadWithUnsupportedExtension", func(t *testing.T) {
		// Create a file with a valid size but an unsupported extension
		fileContent := []byte("This is a valid file")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "unsupported.xyz",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024),
		}

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "unsupported file format")
	})

	t.Run("ValidUploadWithAdditionalFormats", func(t *testing.T) {
		// Create test cases for various valid audio formats
		formats := []struct {
			extension string
			content   []byte
		}{
			{".flac", []byte("FLAC audio data")},
			{".wav", []byte("WAV audio data")},
			{".aiff", []byte("AIFF audio data")},
			// Add more formats here
		}

		for _, format := range formats {
			fileContent := format.content
			dummyFile := NewCustomFile(fileContent)
			dummyFileHeader := &multipart.FileHeader{
				Filename: "audio" + format.extension,
				Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
			}
			expectedISRC := "your_expected_isrc_code"
			mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
			mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

			_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
			// assert.NoError(t, err, "Failed to upload a valid %s file.", format.extension)
		}
	})

	t.Run("ValidUploadWithMinimumDuration", func(t *testing.T) {
		// Create a file with a valid size and supported extension but the minimum duration
		fileContent := []byte("This is a valid MP3 file with minimum duration")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the minimum duration
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "invalid duration")
	})

	t.Run("ValidUploadWithMinimumSampleRate", func(t *testing.T) {
		// Create a file with a valid size and supported extension but the minimum sample rate
		fileContent := []byte("This is a valid MP3 file with minimum sample rate")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the minimum sample rate
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
	})

	t.Run("ValidUploadWithSupportedExtension", func(t *testing.T) {
		// Create a file with a valid size and supported extension
		fileContent := []byte("This is a valid MP3 file")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the expected behavior of the mocks
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("ValidUploadWithMinimumDuration", func(t *testing.T) {
		// Create a file with a valid size and supported extension but the minimum duration
		fileContent := []byte("This is a valid MP3 file with minimum duration")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the minimum duration
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "invalid duration")
	})

	t.Run("ValidUploadWithMinimumSampleRate", func(t *testing.T) {
		// Create a file with a valid size and supported extension but the minimum sample rate
		fileContent := []byte("This is a valid MP3 file with minimum sample rate")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the minimum sample rate
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
	})

	t.Run("ValidUploadWithMinimumDuration", func(t *testing.T) {
		// Create a file with a valid size and supported extension but the minimum duration
		fileContent := []byte("This is a valid MP3 file with minimum duration")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the minimum duration
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "invalid duration")
	})

	t.Run("ValidUploadWithMinimumSampleRate", func(t *testing.T) {
		// Create a file with a valid size and supported extension but the minimum sample rate
		fileContent := []byte("This is a valid MP3 file with minimum sample rate")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the minimum sample rate
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		// Use a deferred function to recover from panic
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("Recovered from panic:", r)
			}
		}()

		_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
		assert.NotNil(t, err)
	})

	t.Run("UploadToS3Error", func(t *testing.T) {
		// Set up a scenario where uploading to S3 fails
		fileContent := []byte("This is a valid MP3 file")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		// Set the expected behavior of the mocks
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("S3 upload error")).AnyTimes()

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)

	})

	t.Run("EmptyFile", func(t *testing.T) {
		fileContent := []byte{}
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "emptyfile.txt",
			Size:     0,
		}
		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)

	})

	t.Run("ValidISRCRetrieval", func(t *testing.T) {
		// Set up the repository mock to return an expected ISRC code
		expectedISRC := "expected_isrc_code"
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)

		fileContent := []byte("This is a valid MP3 file")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024),
		}

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("ValidMP3WithoutMetadata", func(t *testing.T) {
		fileContent := []byte("This is a valid MP3 file without metadata")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "no_metadata.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}

		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return("expectedISRC", nil)
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("ErrorDuringS3Upload", func(t *testing.T) {
		fileContent := []byte("This is a valid MP3 file")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}
		expectedISRC := "your_expected_isrc_code"

		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("S3 upload error")).AnyTimes()

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("ValidMP3WithMaxSupportedPlus1DurationSampleRate", func(t *testing.T) {
		// Create a file with valid size, supported extension, and maximum supported duration and sample rate plus 1
		fileContent := []byte("This is a valid MP3 file with max supported duration and sample rate + 1")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}

		// Set maximum supported duration and sample rate plus 1
		expectedISRC := "your_expected_isrc_code"
		mockRepo.EXPECT().GenerateISRC(gomock.Any(), gomock.Any()).Return(expectedISRC, nil)
		mockRepo.EXPECT().InsertTrackMetadata(gomock.Any(), gomock.Any()).Return(expectedISRC, nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	})

	t.Run("FileSizeZero", func(t *testing.T) {
		dummyFileHeader.Size = 0 // Set the file size to zero
		_, _ = trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)

	})

	fmt.Println("All tests completed successfully")
}

func TestCreateAudioDecoder(t *testing.T) {
	t.Run("SupportedFormats", func(t *testing.T) {
		testCases := map[string]string{
			".flac": "FLAC",
			".aif":  "AIFF",
			".wav":  "WAV",
			".mp3":  "MP3",
			".ogg":  "OGG",
		}

		for ext, format := range testCases {
			t.Run(format, func(t *testing.T) {
				fileContent := []byte("test")
				file := NewCustomFile(fileContent)
				decoder, err := createAudioDecoder(file, ext)
				assert.NoError(t, err)
				assert.NotNil(t, decoder)
			})
		}
	})

	t.Run("UnsupportedFormat", func(t *testing.T) {
		fileContent := []byte("test")
		file := NewCustomFile(fileContent)
		_, err := createAudioDecoder(file, ".xyz")
		assert.Error(t, err)
		assert.Contains(t, err.Error(), "unsupported file format")
	})
}

func TestGetAudioProperties(t *testing.T) {
	fileContent := []byte("test")
	decoder := audio.NewFlacDecoder(audio.DecoderConfig{}).WithBytes(NewCustomFile(fileContent))

	duration, sampleRate, err := getAudioProperties(decoder)
	assert.NoError(t, err)
	assert.Zero(t, duration)
	assert.Zero(t, sampleRate)
}

func TestCheckDurationAndSampleRate(t *testing.T) {
	t.Run("ValidDurationAndSampleRate", func(t *testing.T) {
		duration := 10.0
		sampleRate := 44100.0
		fieldsMap := make(map[string][]string)
		_ = checkDurationAndSampleRate(duration, sampleRate, fieldsMap)
	})

	t.Run("InvalidDuration", func(t *testing.T) {
		duration := 0.5
		sampleRate := 44100.0
		fieldsMap := make(map[string][]string)
		_ = checkDurationAndSampleRate(duration, sampleRate, fieldsMap)

	})

	t.Run("InvalidSampleRate", func(t *testing.T) {
		duration := 10.0
		sampleRate := 1000.0
		fieldsMap := make(map[string][]string)
		_ = checkDurationAndSampleRate(duration, sampleRate, fieldsMap)

	})
}

func TestGetS3Key(t *testing.T) {
	t.Run("FlacFormat", func(t *testing.T) {
		fileExtension := ".flac"
		partnerID := "testPartnerID"
		memberID := "testMemberID"
		trackID := "testTrackID"
		key := getS3Key(fileExtension, partnerID, memberID, trackID)
		expected := fmt.Sprintf("tuneverse/%s/%s/tracks/%s", partnerID, memberID, trackID)
		assert.Equal(t, expected, key)
	})

	t.Run("OtherFormats", func(t *testing.T) {
		fileExtension := ".mp3"
		partnerID := "testPartnerID"
		memberID := "testMemberID"
		trackID := "testTrackID"
		key := getS3Key(fileExtension, partnerID, memberID, trackID)
		expected := fmt.Sprintf("tuneverse/temp/tracks/%s", trackID)
		assert.Equal(t, expected, key)
	})
}

func TestCapitalizeLetters(t *testing.T) {
	t.Run("AllLowerCase", func(t *testing.T) {
		input := "hello"
		result := capitalizeLetters(input)
		assert.Equal(t, "HELLO", result)
	})

	t.Run("MixedCase", func(t *testing.T) {
		input := "HeLLo WoRLd"
		result := capitalizeLetters(input)
		assert.Equal(t, "HELLO WORLD", result)
	})

	t.Run("AllUpperCase", func(t *testing.T) {
		input := "UPPERCASE"
		result := capitalizeLetters(input)
		assert.Equal(t, "UPPERCASE", result)
	})

	t.Run("EmptyString", func(t *testing.T) {
		input := ""
		result := capitalizeLetters(input)
		assert.Equal(t, "", result)
	})
}

func TestExtractFileInfo(t *testing.T) {
	t.Run("ValidFileHeader", func(t *testing.T) {
		fileHeader := &multipart.FileHeader{
			Filename: "sample.txt",
			Size:     1024,
		}
		fileExtension, fileNameWithoutExt, fileSizeInMb, err := ExtractFileInfo(fileHeader)
		assert.NoError(t, err)
		assert.Equal(t, ".txt", fileExtension)
		assert.Equal(t, "sample", fileNameWithoutExt)
		assert.Equal(t, 0.0009765625, fileSizeInMb)
	})
}

func TestCheckFileSize(t *testing.T) {
	t.Run("ValidFileSize", func(t *testing.T) {
		fileSizeInMb := 1.0
		fieldsMap := make(map[string][]string)
		_ = checkFileSize(fileSizeInMb, fieldsMap)
	})

}

func TestUpdateTrackStatus(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock for your repository
	mockRepo := mock.NewMockTrackRepoImply(ctrl)

	// Replace track.repo with the mock in your use case
	trackUseCases := &TrackUseCases{
		repo: mockRepo,
		// Other fields...
	}

	// Define the test parameters
	ctx := context.Background()
	userID := "testUserID"
	trackID := "testTrackID"
	status := 3

	// Set expectations on the mock repository
	mockRepo.EXPECT().UpdateTrackStatus(ctx, userID, trackID, status).Return(nil)

	// Call the function you want to test
	err := trackUseCases.repo.UpdateTrackStatus(ctx, userID, trackID, status)

	// Check if the function returned the expected error (nil in this case)
	if err != nil {
		t.Errorf("UpdateTrackStatus returned an unexpected error: %v", err)
	}
	t.Run("UpdateConvertionStatus_Success", func(t *testing.T) {
		// Set up your mockRepo expectations for a successful update
		mockRepo.EXPECT().UpdateConvertionStatus(ctx, userID, trackID, gomock.Any()).Return(nil)

		// Call the method being tested
		err := trackUseCases.UpdateConvertionStatus(ctx, userID, trackID, entities.UpdateTrackStatusRequest{})

		// Assert that the error is nil, indicating success
		assert.Nil(t, err)
	})

	t.Run("UpdateConvertionStatus_Failure", func(t *testing.T) {
		// Set up your mockRepo expectations for a failed update
		expectedErr := fmt.Errorf("some error")
		mockRepo.EXPECT().UpdateConvertionStatus(ctx, userID, trackID, gomock.Any()).Return(expectedErr)

		// Call the method being tested
		err := trackUseCases.UpdateConvertionStatus(ctx, userID, trackID, entities.UpdateTrackStatusRequest{})

		// Assert that the error matches the expected error
		assert.EqualError(t, err, fmt.Sprintf("failed to update track status: %v", expectedErr))
	})
}

func TestUploadTrackWithEmptyUserID(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	trackUseCases := &TrackUseCases{
		repo:  mockRepo,
		cloud: mockCloud,
	}

	ctx := context.Background()
	userID := "" // Empty user ID
	partnerID := "testPartnerID"
	fileContent := []byte("This is a valid MP3 file")
	dummyFile := NewCustomFile(fileContent)
	dummyFileHeader := &multipart.FileHeader{
		Filename: "valid.mp3",
		Size:     int64(consts.MaxFileSizeMb * 1024 * 1024),
	}

	_, err := trackUseCases.UploadTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader)
	assert.NotNil(t, err)
	assert.Contains(t, err.Error(), "user ID is required")
}

func TestMegabytesToBytes(t *testing.T) {
	megabytes := 10.0
	expectedBytes := int64(megabytes * 1024 * 1024)

	result := megabytesToBytes(megabytes)
	if result != expectedBytes {
		t.Errorf("Expected %d bytes, but got %d", expectedBytes, result)
	}
}

func TestGenerateContentType(t *testing.T) {
	t.Run("KnownExtension", func(t *testing.T) {
		fileName := "audio.mp3"
		contentType := generateContentType(fileName)
		expectedContentType := "audio/mpeg"
		if contentType != expectedContentType {
			t.Errorf("Expected content type %s, but got %s", expectedContentType, contentType)
		}
	})

	t.Run("UnknownExtension", func(t *testing.T) {
		fileName := "audio.xyz"
		contentType := generateContentType(fileName)
		expectedContentType := "application/octet-stream"
		if contentType != expectedContentType {
			t.Errorf("Expected content type %s, but got %s", expectedContentType, contentType)
		}
	})
}

func TestUploadTrack_InsertTrackMetadata_Success(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Initialize your TrackUseCases and relevant dependencies here
	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	trackUseCases := &TrackUseCases{
		repo:  mockRepo,
		cloud: mockCloud,
	}

	ctx := context.TODO()
	userID := "user123"

	// Create a sample TrackMetadata entity
	trackMetadata := entities.TrackMetadata{

		Name:       "sample_track",
		FileName:   "sample_track",
		Duration:   3.45,
		CreatedOn:  time.Now(),
		MemberID:   userID,
		Status:     1,
		FileSize:   megabytesToBytes(1.0),
		FileExt:    "mp3",
		SampleRate: 44100,
		ISRC:       "ISRC123",
	}

	mockRepo.EXPECT().
		InsertTrackMetadata(ctx, gomock.Any()).
		Return(trackMetadata.Name, nil)

	// Set the mock repository for your TrackUseCases
	trackUseCases.repo = mockRepo

	_, _ = trackUseCases.repo.InsertTrackMetadata(ctx, trackMetadata)

}

func TestUpdateTrackStatus_Success(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Initialize your TrackUseCases and relevant dependencies here
	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	trackUseCases := &TrackUseCases{
		repo:  mockRepo,
		cloud: mockCloud,
	}

	ctx := context.TODO()
	userID := "user123"
	trackID := "track123"
	status := 2

	// Mock the repository's UpdateTrackStatus function
	mockRepo.EXPECT().
		UpdateTrackStatus(ctx, userID, trackID, status).
		Return(nil)

	// Set the mock repository for your TrackUseCases
	trackUseCases.repo = mockRepo

	err := trackUseCases.repo.UpdateTrackStatus(ctx, userID, trackID, status)

	assert.NoError(t, err)
}

func TestUploadToS3AndHandleError(t *testing.T) {
	// Create a new GoMock controller
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create mocks for your dependencies
	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	// Create an instance of your TrackUseCases with the mock dependencies
	trackUseCases := &TrackUseCases{
		repo:  mockRepo,
		cloud: mockCloud,
	}

	// Define test data
	ctx := context.TODO()
	userID := "testUserID"
	trackID := "testTrackID"
	fileExtension := ".mp3"
	bucketName := "testBucket"
	key := "testKey"
	contentType := "audio/mpeg"
	fileHeader := &multipart.FileHeader{
		Filename: "testfile.mp3",
		Size:     1024,
	}

	// Set up expectations for your mock objects
	mockCloud.EXPECT().UploadToS3(bucketName, key, fileHeader, contentType).Return(nil)
	mockRepo.EXPECT().UpdateTrackStatus(ctx, userID, trackID, 2).Return(nil)

	// Call the method you want to test
	err := trackUseCases.UploadToS3AndHandleError(ctx, userID, trackID, fileExtension, bucketName, key, contentType, fileHeader)

	// Check if there were any errors
	if err != nil {
		t.Errorf("Expected no error, but got: %v", err)
	}
}

func TestUpdateTrackMetadata(t *testing.T) {

	genreID1, genreID2 := uuid.New(), uuid.New()
	artistID := uuid.New()
	artist := map[string]interface{}{"id": artistID.String(), "is_featured": false}
	trackID, memberID := uuid.NewString(), uuid.NewString()
	partnerID := uuid.NewString()
	roleIds := uuid.New()
	participants := []any{map[string]interface{}{"id": artistID, "role_id": roleIds, "percentage": 15}}

	envConfig := &entities.EnvConfig{
		AWS: entities.AWS{
			BucketName: "tuneverse",
		},
	}

	trackMetadata := entities.TrackProperties{
		"id":             trackID,
		"name":           "Sample Track",
		"genres":         []any{genreID1.String(), genreID2.String()},
		"isrc":           "INISR1234567",
		"iswc":           "T0345246801",
		"language":       "en",
		"explicit":       float64(0),
		"allow_download": true,
		"artists":        []any{artist},
		"participants":   participants,
		//"featured_artists": []any{artistID.String()},
		"album_only":       false,
		"lyrics":           "Sample lyrics...",
		"preview_start_at": float64(0),
		"preview_duration": float64(15),
		"is_instrumental":  false,
		"member_id":        memberID,
	}

	testCases := []struct {
		name          string
		arg           entities.TrackProperties
		partnerID     string
		buildStubs    func(store *mock.MockTrackRepoImply, arg entities.TrackProperties)
		checkResponse func(t *testing.T, validationErrors map[string][]string, err error)
	}{
		{
			name:      "ok",
			arg:       trackMetadata,
			partnerID: partnerID,
			buildStubs: func(store *mock.MockTrackRepoImply, arg entities.TrackProperties) {
				genres, err := trackMetadata.GetGenres()
				require.Nil(t, err)
				artists, _, err := trackMetadata.GetArtists()
				require.Nil(t, err)

				store.EXPECT().
					ValidateTrackOwner(gomock.Any(), gomock.Eq(trackMetadata.GetTrackID()), gomock.Eq(trackMetadata.GetMemberID())).
					Times(1).
					Return(true, nil)

				store.EXPECT().
					GetID(
						gomock.Any(), gomock.Eq(consts.TableLanguage),
						gomock.Eq(consts.LangCode), gomock.Eq(trackMetadata.GetLangCode()), gomock.Eq(consts.LangCode),
					).Times(1).Return(true, nil)

				store.EXPECT().
					VerifyTrackGenres(gomock.Any(), gomock.Eq(partnerID), gomock.Eq(genres)).
					Times(1).Return([]uuid.UUID{genreID1, genreID2}, nil)

				store.EXPECT().
					VerifyArtists(gomock.Any(), gomock.Eq(memberID), gomock.Eq(artists)).
					Times(2).Return([]uuid.UUID{artistID}, nil)

				store.EXPECT().
					VerifyRoles(gomock.Any(), gomock.Eq(partnerID), gomock.Eq([]uuid.UUID{roleIds})).
					Times(1).Return([]uuid.UUID{roleIds}, nil)

				store.EXPECT().
					UpdateTrackTx(gomock.Any(), trackMetadata).
					Times(1).Return(int64(1), nil)

			},
			checkResponse: func(t *testing.T, validationErrors map[string][]string, err error) {
				require.Nil(t, validationErrors)
				require.Nil(t, err)

			},
		},
		{
			name: "invalid arguments",
			arg: entities.TrackProperties{
				"id":               trackID,
				"name":             "",
				"genres":           []any{genreID1.String(), genreID2.String()},
				"isrc":             "INISR",
				"iswc":             "T0345",
				"language":         "",
				"explicit":         float64(-1),
				"artists":          []any{artist},
				"participants":     participants,
				"featured_artists": []any{artistID.String()},
				"album_only":       false,
				"lyrics":           "Sample lyrics...",
				"preview_start_at": float64(-1),
				"preview_duration": float64(-1),
				"is_instrumental":  false,
				"member_id":        memberID,
			},
			partnerID: partnerID,
			buildStubs: func(store *mock.MockTrackRepoImply, arg entities.TrackProperties) {
				genres, err := trackMetadata.GetGenres()
				require.Nil(t, err)
				artists, _, err := trackMetadata.GetArtists()
				require.Nil(t, err)
				store.EXPECT().
					ValidateTrackOwner(gomock.Any(), gomock.Eq(trackID), gomock.Eq(memberID)).
					Times(1).
					Return(true, nil)

				store.EXPECT().
					GetID(
						gomock.Any(), gomock.Eq(consts.TableLanguage),
						gomock.Eq(consts.LangCode), gomock.Eq(""), gomock.Eq(consts.LangCode),
					).Times(1).Return(false, sql.ErrNoRows)

				store.EXPECT().
					VerifyTrackGenres(gomock.Any(), gomock.Eq(partnerID), gomock.Eq(genres)).
					Times(1).Return(nil, consts.ErrNotFound)

				store.EXPECT().
					VerifyArtists(gomock.Any(), gomock.Eq(memberID), gomock.Eq(artists)).
					Times(2).Return(nil, consts.ErrNotFound)

				store.EXPECT().
					VerifyRoles(gomock.Any(), gomock.Eq(partnerID), gomock.Eq([]uuid.UUID{roleIds})).
					Times(1).Return(nil, consts.ErrNotFound)

				store.EXPECT().
					UpdateTrackTx(gomock.Any(), arg).
					Times(0)

			},
			checkResponse: func(t *testing.T, validationErrors map[string][]string, err error) {
				require.Greater(t, len(validationErrors), 0)
				require.Nil(t, err)
			},
		},
		{
			name:      "track owner validation failed",
			arg:       trackMetadata,
			partnerID: partnerID,
			buildStubs: func(store *mock.MockTrackRepoImply, arg entities.TrackProperties) {
				store.EXPECT().
					ValidateTrackOwner(gomock.Any(), gomock.Eq(arg.GetTrackID()), gomock.Eq(arg.GetMemberID())).
					Times(1).
					Return(false, nil)
			},
			checkResponse: func(t *testing.T, validationErrors map[string][]string, err error) {
				require.Nil(t, validationErrors)
				require.NotNil(t, err)
			},
		},
		{
			name: "invalid track id",
			arg: entities.TrackProperties{
				"id": "",
			},
			partnerID:  partnerID,
			buildStubs: func(store *mock.MockTrackRepoImply, arg entities.TrackProperties) {},
			checkResponse: func(t *testing.T, validationErrors map[string][]string, err error) {
				require.Nil(t, validationErrors)
				require.NotNil(t, err)

			},
		},
		{
			name:      "invalid genre",
			arg:       trackMetadata,
			partnerID: partnerID,
			buildStubs: func(store *mock.MockTrackRepoImply, arg entities.TrackProperties) {
				genres, err := trackMetadata.GetGenres()
				require.Nil(t, err)
				store.EXPECT().
					ValidateTrackOwner(gomock.Any(), gomock.Eq(arg.GetTrackID()), gomock.Eq(arg.GetMemberID())).
					Times(1).
					Return(true, nil)

				store.EXPECT().
					GetID(
						gomock.Any(), gomock.Eq(consts.TableLanguage),
						gomock.Eq(consts.LangCode), gomock.Eq(arg.GetLangCode()), gomock.Eq(consts.LangCode),
					).Times(1).Return(true, nil)

				store.EXPECT().
					VerifyTrackGenres(gomock.Any(), gomock.Eq(partnerID), gomock.Eq(genres)).
					Times(1).Return(nil, sql.ErrConnDone)
			},
			checkResponse: func(t *testing.T, validationErrors map[string][]string, err error) {
				require.Nil(t, validationErrors[consts.TrackGenres])
				require.NotNil(t, err)
			},
		},
	}

	for i := range testCases {
		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mock.NewMockTrackRepoImply(ctrl)
			cloud := mockcloud.NewMockCloudServiceImply(ctrl)
			mockQueue := &mockqueue.MockQueue{}
			tc.buildStubs(store, tc.arg)
			trackUsecases := NewTrackUseCases(store, cloud, envConfig, mockQueue)
			validationErrors, err := trackUsecases.UpdateTrackMetadata(context.Background(), tc.arg, string(tc.partnerID))
			tc.checkResponse(t, validationErrors, err)
		})
	}

}

// Close method to satisfy the multipart.File interface.
func (cf *CustomFile) Close() error {
	return nil // No need to do anything in Close for a reader.
}

// Seek method to satisfy the multipart.File interface.
func (cf *CustomFile) Seek(offset int64, whence int) (int64, error) {
	return cf.Reader.Seek(offset, whence)
}

// Size method to satisfy the multipart.File interface.
func (cf *CustomFile) Size() int64 {
	return cf.size
}

// TestUpdateTrack tests updateTrack function
func TestUpdateTrack(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	envConfig := entities.EnvConfig{
		AWS: entities.AWS{
			BucketName: "tuneverse",
		},
	}

	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)
	mockQueue := &mockqueue.MockQueue{}

	trackUseCases := NewTrackUseCases(mockRepo, mockCloud, &envConfig, mockQueue)

	ctx := context.Background()
	userID := "testUserID"
	partnerID := "testPartnerID"
	trackID := "testTrackID"
	file, err := os.Open("../sample/audio.mp3")
	require.Nil(t, err)
	fileInfo, err := file.Stat()
	require.Nil(t, err)

	fileHeader := &multipart.FileHeader{
		Filename: fileInfo.Name(),
		Size:     fileInfo.Size(),
	}

	//This tests the succesfull scenerio
	t.Run("ValidUpload", func(t *testing.T) {

		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().UpdateTrackStatus(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Times(1).Return(nil)
		mockRepo.EXPECT().UpdateTrackMetadata(gomock.Any(), gomock.Any()).Times(1).Return(nil)
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Times(1).Return(nil)

		_, err := trackUseCases.UpdateTrack(ctx, userID, partnerID, file, fileHeader, trackID)
		require.Nil(t, err)
	})

	//This est the succesfull sS3 upload
	t.Run("SuccessUploadToS3", func(t *testing.T) {

		fileContent := []byte("This is a valid MP3 file with minimum sample rate")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "valid.mp3",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024),
		}

		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().UpdateTrackMetadata(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
		mockCloud.EXPECT().UploadToS3(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()

		_, err := trackUseCases.UpdateTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader, trackID)
		require.NotNil(t, err)
	})

	// This test covers the scenario where the file size exceeds the maximum allowed size.
	t.Run("InvalidFileSize", func(t *testing.T) {

		fileContent := []byte("This file is too large")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "large.mp3",
			Size:     int64((consts.MaxFileSizeMb + 1) * 1024 * 1024), // Exceeds maximum size
		}

		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)

		fieldsMap, err := trackUseCases.UpdateTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader, trackID)
		require.NotNil(t, err)
		expectedFieldsMap := map[string][]string{"file_size": {"format"}}
		require.Equal(t, fieldsMap, expectedFieldsMap)
	})

	// This test covers the scenario where the file extension is not supported.
	t.Run("InvalidFileExtension", func(t *testing.T) {
		// Create a file with an unsupported file extension
		fileContent := []byte("This is an unsupported file type")
		dummyFile := NewCustomFile(fileContent)
		dummyFileHeader := &multipart.FileHeader{
			Filename: "unsupported.xyz",
			Size:     int64(consts.MaxFileSizeMb * 1024 * 1024), // Valid file size
		}

		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)

		fieldsMap, err := trackUseCases.UpdateTrack(ctx, userID, partnerID, dummyFile, dummyFileHeader, trackID)
		require.NotNil(t, err)
		expectedFieldsMap := map[string][]string{"common_message": {"unsupported_file"}}
		require.Equal(t, fieldsMap, expectedFieldsMap)
	})

}

// TestGetTrack is a unit test function for the GetTrack method.
func TestGetTrack(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	envConfig := entities.EnvConfig{
		AWS: entities.AWS{
			BucketName: "tuneversev2",
		},
	}
	// Create mock instances
	mockRepo := mock.NewMockTrackRepoImply(ctrl)
	mockCloud := mockcloud.NewMockCloudServiceImply(ctrl)

	ctx := context.Background()
	mockQueue := &mockqueue.MockQueue{}

	trackUseCases := NewTrackUseCases(mockRepo, mockCloud, &envConfig, mockQueue)

	//This test covers the scenario where track data is successfully retrieved with a complete status.
	t.Run("SuccessfulUploadForCompleteStatus", func(t *testing.T) {
		expectedBucket := "tuneversev2"
		expectedKey := "temp/tracks/dded9292-5202-495b-8fd1-e8bfd0995c06"

		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().GetTrack(gomock.Any(), gomock.Any()).Return(entities.Track{
			ID:        "dded9292-5202-495b-8fd1-e8bfd0995c06",
			Status:    "completed",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		}, nil)
		mockCloud.EXPECT().GetObject(ctx, expectedBucket, expectedKey).Return("sample_url", nil)

		fieldsMap, trackData, err := trackUseCases.GetTrack(context.Background(), entities.Track{
			ID:        "dded9292-5202-495b-8fd1-e8bfd0995c06",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		})

		require.Nil(t, err)
		require.Empty(t, fieldsMap)
		require.Equal(t, "sample_url", trackData.URL)
	})

	// // This test covers the scenario where track data is successfully retrieved with a Success status.
	t.Run("SuccessfulUpload", func(t *testing.T) {
		expectedBucket := "tuneversev2"
		expectedKey := "614608f2-6538-4733-aded-96f902007254/980e783c-e664-452d-b1ff-30d2e7767023/tracks/someTrackID"
		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().GetTrack(gomock.Any(), gomock.Any()).Return(entities.Track{
			ID:        "someTrackID",
			Status:    "success",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		}, nil)
		mockCloud.EXPECT().GetObject(ctx, expectedBucket, expectedKey).Return("sample_url", nil)

		fieldsMap, trackData, err := trackUseCases.GetTrack(context.Background(), entities.Track{
			ID:        "someTrackID",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		})

		require.Nil(t, err)
		require.Empty(t, fieldsMap)
		require.Equal(t, "sample_url", trackData.URL)
	})

	// This test covers the scenario where GetTrack fails and returns an error.
	t.Run("FailedfulUpload", func(t *testing.T) {
		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().GetTrack(gomock.Any(), gomock.Any()).Return(entities.Track{}, errors.New("SomeError"))

		fieldsMap, trackData, err := trackUseCases.GetTrack(context.Background(), entities.Track{
			ID:        "someTrackID",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		})

		require.NotNil(t, err)
		require.Empty(t, trackData)
		require.Empty(t, fieldsMap)
	})

	// This test covers the scenario where GetObject fails and returns an error.
	t.Run("FailedUrl", func(t *testing.T) {
		expectedBucket := "tuneversev2"
		expectedKey := "614608f2-6538-4733-aded-96f902007254/980e783c-e664-452d-b1ff-30d2e7767023/tracks/someTrackID"
		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil)
		mockRepo.EXPECT().GetTrack(gomock.Any(), gomock.Any()).Return(entities.Track{
			ID:        "someTrackID",
			Status:    "success",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		}, nil)
		mockCloud.EXPECT().GetObject(ctx, expectedBucket, expectedKey).Return("sample_url", errors.New("SomeError"))

		fieldsMap, _, err := trackUseCases.GetTrack(context.Background(), entities.Track{
			ID:        "someTrackID",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		})

		require.NotNil(t, err)
		require.Nil(t, fieldsMap)
	})

	// This test covers the scenario where ValidateTrackOwner returns false.
	t.Run("InvalidMemberTrackOwner", func(t *testing.T) {
		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(false, nil)

		fieldsMap, trackData, err := trackUseCases.GetTrack(context.Background(), entities.Track{
			ID:        "f34565a5-780b-419b-991b-07eecd5b850b",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "0238d40d-ab2b-4903-a597-a17108c02077",
		})

		require.Nil(t, err)
		require.Empty(t, trackData)
		expectedFieldsMap := map[string][]string{"common_message": {"invalid_track_member"}}
		require.Equal(t, fieldsMap, expectedFieldsMap)
	})

	// This test covers the scenario where ValidateTrackOwner returns false and an error.
	t.Run("ErrorMemberTrackOwner", func(t *testing.T) {
		mockRepo.EXPECT().ValidateTrackOwner(gomock.Any(), gomock.Any(), gomock.Any()).Return(false, errors.New("SomeError"))

		fieldsMap, trackData, err := trackUseCases.GetTrack(context.Background(), entities.Track{
			ID:        "151052d7-99f7-4c91-9280-4575e4de1a13",
			PartnerID: "614608f2-6538-4733-aded-96f902007254",
			MemberID:  "980e783c-e664-452d-b1ff-30d2e7767023",
		})

		require.NotNil(t, err)
		require.Empty(t, trackData)
		require.Empty(t, fieldsMap)
	})

}

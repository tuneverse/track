package usecases

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"mime"
	"mime/multipart"
	"path/filepath"
	"strings"
	"time"
	cloud "track/internal/cloud/awsutils"
	"track/internal/consts"
	"track/internal/entities"
	"track/internal/repo"
	"track/utilities"

	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/queue"
	"gitlab.com/tuneverse/toolkit/utils"
	"gitlab.com/tuneverse/toolkit/utils/audio"
)

// TrackUseCases represents use cases for handling Track-related operations.
type TrackUseCases struct {
	repo  repo.TrackRepoImply
	cloud cloud.CloudServiceImply
	env   *entities.EnvConfig
	queue queue.Queue
}

// TrackUseCaseImply is an interface defining the methods for working with Track use cases.
type TrackUseCaseImply interface {
	DeleteTrack(ctx context.Context, trackId, memberId, partnerId string) (map[string][]string, error)
	GetTrack(context.Context, entities.Track) (map[string][]string, entities.Track, error)
	UpdateTrack(ctx context.Context, memberID string, partnerID string, file multipart.File, fileHeader *multipart.FileHeader, trackID string) (map[string][]string, error)
	UploadTrack(ctx context.Context, userID string, PartnerID string, file multipart.File, fileHeader *multipart.FileHeader) (map[string][]string, error)
	UpdateTrackMetadata(ctx context.Context, arg entities.TrackProperties, partnerID string) (map[string][]string, error)
	ValidateTrackMetadata(arg entities.TrackProperties) (map[string][]string, error)
	UpdateConvertionStatus(ctx context.Context, userID string, trackID string, reqBody entities.UpdateTrackStatusRequest) error
}

// NewTrackUseCases creates a new TrackUseCases instance.
func NewTrackUseCases(TrackRepo repo.TrackRepoImply, cloud cloud.CloudServiceImply, env *entities.EnvConfig, queue queue.Queue) TrackUseCaseImply {
	return &TrackUseCases{
		repo:  TrackRepo,
		cloud: cloud,
		env:   env,
		queue: queue,
	}
}

// DeleteTrack deletes a track and performs necessary checks and actions.
//
// Parameters:
//
//	@ctx: The context for the database operation.
//	@trackId: The unique identifier for the track to be deleted.
//	@memberId: The unique identifier for the member who owns the track.
//	@partnerId: The unique identifier for the partner.
//
// Returns:
//
//	@fieldsMap: A map of fields used for reporting. It may contain error messages.
//	@err: An error, if any, encountered during the deletion process.
//
// Functionality:
//   - Validates track ownership to ensure the track exists and the member has ownership.
//   - Checks if the track is assigned to a product.
//   - Retrieves the file extension for the track.
//   - Gets the ISRC for the track.
//   - Deletes the object from cloud storage.
//   - Deletes the track from the repository.
//   - Updates the ISRC status.
//     Any errors encountered during these steps are logged, and relevant error messages are added to the fieldsMap.
func (track *TrackUseCases) DeleteTrack(ctx context.Context, trackId, memberId, partnerId string) (map[string][]string, error) {
	fieldsMap := map[string][]string{}
	log := logger.Log().WithContext(ctx)
	err := track.repo.ValidateTrackOwnership(ctx, trackId, memberId)
	if err != nil {
		fmt.Println("track failed not found", err)
		utils.AppendValuesToMap(fieldsMap, "common_message", "track_Not_Found")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
		return fieldsMap, err
	}
	err = track.repo.CheckTrackIsAssignedToProduct(ctx, trackId)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "product_Not_Assigned")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
		return fieldsMap, err
	}
	ext, err := track.repo.GetFileExtension(ctx, trackId, memberId)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "track_Not_Found")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
		return fieldsMap, err
	}
	isrc, err := track.repo.GetISRCByTrackID(ctx, trackId)
	if err != nil {
		fmt.Println(err)
		utils.AppendValuesToMap(fieldsMap, "common_message", "deletion_Failed")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
		return fieldsMap, err
	}
	var key string
	if ext == "flac" {
		key = fmt.Sprintf("tuneverse/%s/%s/tracks/%s", partnerId, memberId, trackId)
	} else {
		key = fmt.Sprintf("tuneverse/temp/tracks/%s", trackId)
	}
	bucketName := track.env.AWS.BucketName
	err = track.cloud.DeleteObject(ctx, bucketName, key)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "deletion_Failed")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
	}
	err = track.repo.DeleteTrack(ctx, trackId)
	if err != nil {
		fmt.Println(err)
		utils.AppendValuesToMap(fieldsMap, "common_message", "deletion_Failed")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
		return fieldsMap, err
	}
	err = track.repo.UpdateIsrcStatus(ctx, isrc)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "deletion_Failed")
		log.Errorf("DeleteTrack Failed, err%s", err.Error())
		return fieldsMap, err
	}
	return fieldsMap, err
}

// UpdateTrackMetadata updates the metadata of a track and performs various validations.
//
// Parameters:
//
//	@ctx: The context for the database transaction.
//	@arg: An instance of entities.TrackProperties containing the updated track metadata.
//	@partnerID: The ID of the partner associated with the track.
//
// Behavior:
//   - Validates the input track ID and checks if the track is associated with the member.
//   - Validates various aspects of track metadata, including language, genres, artists, featured artists, and participants.
//   - Performs checks and validations for each aspect and accumulates validation errors if any.
//   - If there are validation errors, it returns the validation errors and an appropriate error message.
//   - If all validations pass, it updates the track metadata using the repository.
//
// Returns:
//   - A map of validation errors if any exist.
//   - An error if there are issues with the request or data.
//   - Nil validation errors and nil error if the track metadata is successfully updated.
//
// Example:
//   - validationErrors, err := UpdateTrackMetadata(ctx, arg, partnerID)
//   - if err != nil {
//     // Handle the error
//     }
//   - if validationErrors != nil {
//     // Handle the validation errors
//     }
//   - // Track metadata is successfully updated.
func (track *TrackUseCases) UpdateTrackMetadata(ctx context.Context, arg entities.TrackProperties, partnerID string) (map[string][]string, error) {

	var validationErrors map[string][]string
	memberID := arg.GetMemberID()
	lg := logger.Log().WithContext(ctx)
	if err := arg.ParseTrackID(); err != nil {
		lg.Errorf("UpdateTrackMetadata: usecases err=%s", err.Error())
		return nil, consts.ErrNotFound
	}

	// Check if the track is associated with the member.
	isTrackExists, err := track.repo.ValidateTrackOwner(ctx, arg.GetTrackID(), memberID)
	if err != nil {
		return nil, err
	}
	if !isTrackExists {
		return nil, consts.ErrNotFound
	}
	// Validate the track metadata.
	validationErrors, err = track.ValidateTrackMetadata(arg)
	if err != nil {
		return nil, err
	}

	//verifies the input language
	if arg.KeyExists("language") {
		if languageCode, err := track.repo.GetID(ctx, consts.TableLanguage, consts.LangCode, arg.GetLangCode(), consts.LangCode); err != nil {
			if lang, ok := languageCode.(string); !ok || utils.IsEmpty(lang) || errors.Is(err, sql.ErrNoRows) {
				utils.AppendValuesToMap(validationErrors, consts.TrackLanguage, consts.NotExists)
			} else {
				lg.Errorf("UpdateTrackMetadata: failed to get language err=%s", err.Error())
				return nil, err
			}
		}
	}

	// Verify the track genres.
	if arg.KeyExists("genres") {
		genres, err := arg.GetGenres()
		if err != nil {
			utils.AppendValuesToMap(validationErrors, consts.TrackGenres, consts.NotExists)
		} else if length := len(genres); length < 1 || length > 10 {
			utils.AppendValuesToMap(validationErrors, consts.TrackGenres, consts.GenreCount)
		} else {
			if _, err = track.repo.VerifyTrackGenres(ctx, partnerID, genres...); err != nil {
				if !errors.Is(err, consts.ErrNotFound) {
					lg.Errorf("UpdateTrackMetadata: usecases: VerifyTrackGenres err=%s", err.Error())
					return nil, err
				}
				utils.AppendValuesToMap(validationErrors, consts.TrackGenres, consts.NotExists)
			}
		}

	}
	// Verify the track artists.
	if arg.KeyExists("artists") {
		artists, featuredArtists, err := arg.GetArtists()
		if err != nil {
			utils.AppendValuesToMap(validationErrors, consts.Artists, consts.NotExists)
		} else if len(artists) < 1 {
			utils.AppendValuesToMap(validationErrors, consts.Artists, consts.ArtistCount)
		} else {
			allArtist := make([]uuid.UUID, len(artists)+len(featuredArtists))
			copy(allArtist, artists)
			copy(allArtist[len(artists):], featuredArtists)
			if _, err := track.repo.VerifyArtists(ctx, memberID, allArtist...); err != nil {
				if !errors.Is(err, consts.ErrNotFound) {
					lg.Errorf("UpdateTrackMetadata: usecases: VerifyArtists err=%s", err.Error())
					return nil, err
				}
				utils.AppendValuesToMap(validationErrors, consts.Artists, consts.NotExists)
			}
		}
	}

	// Verify the featured artists.
	// if arg.KeyExists("featured_artists") {
	// 	featuredArtists, err := arg.GetFeaturedArtists()
	// 	if err != nil {
	// 		utils.AppendValuesToMap(validationErrors, consts.FeaturedArtists, consts.NotExists)
	// 	} else if featuredArtists != nil {
	// 		if _, err := track.repo.VerifyArtists(ctx, memberID, featuredArtists...); err != nil {
	// 			if !errors.Is(err, consts.ErrNotFound) {
	// 				lg.Errorf("UpdateTrackMetadata: usecases: VerifyArtists err=%s", err.Error())
	// 				return nil, err
	// 			}
	// 			utils.AppendValuesToMap(validationErrors, consts.FeaturedArtists, consts.NotExists)
	// 		}
	// 	}
	// }

	// Verify the participants.
	if arg.KeyExists("participants") {
		participants, err := arg.GetParticipants()
		if err != nil {
			utils.AppendValuesToMap(validationErrors, consts.Participants, consts.InvalidData)
		} else if len(participants) < 1 {
			utils.AppendValuesToMap(validationErrors, consts.Participants, consts.ParticipantCount)
		} else {
			var (
				partcipantsID []uuid.UUID
				roleIds       []uuid.UUID
			)
			for _, v := range participants {
				partcipantsID = append(partcipantsID, v.ID)
				roleIds = append(roleIds, v.RoleID)
				if v.Percentage < 0 || v.Percentage > 100 {
					utils.AppendValuesToMap(validationErrors, consts.Participants, consts.InvalidPercentage)
				}
			}
			// Verify the participant IDs.
			_, err := track.repo.VerifyArtists(ctx, memberID, partcipantsID...)
			if err != nil {
				if !errors.Is(err, consts.ErrNotFound) {
					lg.Errorf("UpdateTrackMetadata: usecases: VerifyArtists err=%s", err.Error())
					return nil, err
				}
				utils.AppendValuesToMap(validationErrors, consts.Participants, consts.NotExists)
			}
			// Verify the participant roles.
			_, err = track.repo.VerifyRoles(ctx, partnerID, roleIds...)
			if err != nil {
				if !errors.Is(err, consts.ErrNotFound) {
					lg.Errorf("UpdateTrackMetadata: usecases: VerifyRoles err=%s", err.Error())
					return nil, err
				}
				utils.AppendValuesToMap(validationErrors, consts.Participants, consts.NoRoles)
			}
		}

	}
	// If there are validation errors, return them.
	if len(validationErrors) > 0 {
		return validationErrors, nil
	}
	// Update the track metadata.
	_, err = track.repo.UpdateTrackTx(ctx, arg)
	if err != nil {
		lg.Errorf("UpdateTrackMetadata: usecases: UpdateTrackTx err=%s", err.Error())
		return nil, err
	}
	// Return no validation errors and no error.
	return nil, nil
}

// ValidateTrackMetadata validates the metadata of a track.
//
// Parameters:
//
//	@arg: An instance of entities.TrackProperties containing the track metadata to validate.
//
// Behavior:
//   - Validates different fields of track metadata, including the track name, explicit status, language, ISRC, ISWC,
//     preview start time, and preview duration.
//   - Checks field-specific validation rules and accumulates validation errors if any.
//
// Returns:
//   - A map of validation errors if any exist.
//   - An error if there are validation errors due to invalid data.
//   - Nil validation errors and nil error if the track metadata is successfully validated.
//
// Example:
//   - validationErrors, err := ValidateTrackMetadata(arg)
//   - if err != nil {
//     // Handle the error
//     }
//   - if validationErrors != nil {
//     // Handle the validation errors
//     }
//   - // Track metadata is successfully validated.
func (track *TrackUseCases) ValidateTrackMetadata(arg entities.TrackProperties) (map[string][]string, error) {

	validationErrors := make(map[string][]string)

	for argField := range arg {
		if !consts.Track[argField] {
			return nil, consts.ErrInvalidInput
		}
		switch argField {
		case "name":
			trackName := arg.GetTrackName()
			if utils.IsEmpty(trackName) {
				utils.AppendValuesToMap(validationErrors, consts.TrackName, consts.Required)
			}
			if err := utilities.ValidateStringLength(trackName, 3, 100); err != nil {
				utils.AppendValuesToMap(validationErrors, consts.TrackName, consts.Length)
			}
		case "explicit":
			explicit := arg.GetExplicit()
			if explicit < 0 || explicit > 2 {
				utils.AppendValuesToMap(validationErrors, consts.TrackExplicit, consts.Invalid)
			}
		case "language":
			languageLen := len(arg.GetLangCode())
			if languageLen < 1 || languageLen > 3 {
				utils.AppendValuesToMap(validationErrors, consts.TrackLanguage, consts.Invalid)
			}
		case "isrc":
			if !utilities.ValidateISRC(arg.GetISRC()) {
				utils.AppendValuesToMap(validationErrors, consts.ISRC, consts.Invalid)
			}
		case "iswc":
			if !utilities.ValidateISWC(arg.GetISWC()) {
				utils.AppendValuesToMap(validationErrors, consts.ISWC, consts.Invalid)
			}
		case "preview_start_at":
			previewStart := arg.GetPreviewStartAt()
			if previewStart < 0 {
				utils.AppendValuesToMap(validationErrors, "preview_start_at", consts.Invalid)
			}
		case "preview_duration":
			previewDuration := arg.GetPreviewDuration()
			if previewDuration < 5 {
				utils.AppendValuesToMap(validationErrors, "preview_duration", consts.Invalid)
			}
		}

	}
	return validationErrors, nil

}

// UpdateTrack updates the track with new audio data and metadata.
//
// Parameters:
//
//	@ctx: The context for the update operation.
//	@memberID: The ID of the member updating the track.
//	@partnerID: The ID of the partner associated with the track.
//	@file: The multipart.File containing the new audio data.
//	@fileHeader: The multipart.FileHeader containing file information.
//	@trackID: The ID of the track to update.
//
// Behavior:
//   - Validates if the member updating the track is the owner.
//   - Validates the file format and file size.
//   - Extracts audio information from the uploaded file.
//   - Decodes the audio file to extract its properties like duration and sample rate.
//   - Validates the audio duration and sample rate.
//   - Updates the track metadata with the new information.
//   - Uploads the audio data to an S3 bucket.
//   - Handles any errors during the update process.
//
// Returns:
//   - A map of validation errors if any exist.
//   - An error if there are issues with the update or data.
//   - Nil validation errors and nil error if the track is successfully updated.
//
// Example:
//   - fieldsMap, err := UpdateTrack(ctx, memberID, partnerID, file, fileHeader, trackID)
//   - if err != nil {
//     // Handle the error
//     }
//   - if len(fieldsMap) > 0 {
//     // Handle the validation errors
//     }
//   - // Track is successfully updated.
func (track *TrackUseCases) UpdateTrack(ctx context.Context, memberID string, partnerID string, file multipart.File, fileHeader *multipart.FileHeader, trackID string) (map[string][]string, error) {

	log := logger.Log().WithContext(ctx)
	// Initialize fields map to hold validation errors
	fieldsMap := map[string][]string{}

	// Validate if the track owner is valid
	isValid, err := track.repo.ValidateTrackOwner(ctx, trackID, memberID)

	if err != nil {

		log.Errorf("ValidateTrackOwner Failed, %s", err.Error())
		return nil, err
	}

	if !isValid {

		log.Errorf("Validation failed, ValidateTrackOwner failed")
		utils.AppendValuesToMap(fieldsMap, "common_message", "invalid_track_member")
		return fieldsMap, nil
	}

	// To extracts file information from a multipart file header.
	fileExtension, fileNameWithoutExt, fileSizeInMb, err := ExtractFileInfo(fileHeader)

	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "unsupported_file")
		return fieldsMap, fmt.Errorf("unsupported file format")
	}

	// Check the file size
	if err = checkFileSize(fileSizeInMb, fieldsMap); err != nil {
		return fieldsMap, err
	}

	//To creates an audio decoder based on the file extension.
	decoder, err := createAudioDecoder(file, fileExtension)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "unsupported_file")
		return fieldsMap, fmt.Errorf("unsupported file format")
	}

	//decode audio file
	decoder, err = decodeAudio(decoder)
	if err != nil {
		return fieldsMap, err
	}

	// To extracts duration and sample rate from the audio decoder.
	duration, sampleRate, err := getAudioProperties(decoder)
	if err != nil {
		return fieldsMap, fmt.Errorf("error getting file duration, samplerate: %v", err)
	}

	// Check the sample rate and duration
	if err = checkDurationAndSampleRate(duration, sampleRate, fieldsMap); err != nil {
		return fieldsMap, err
	}

	// Create track metadata
	metadata := entities.TrackMetadata{
		ID:         trackID,
		FileName:   fileNameWithoutExt,
		Duration:   duration,
		UpdatedOn:  time.Now(),
		Status:     1,
		FileSize:   megabytesToBytes(fileSizeInMb),
		FileExt:    fileExtension[1:],
		SampleRate: int(sampleRate),
	}

	// update track metadata
	err = track.repo.UpdateTrackMetadata(ctx, metadata)

	if err != nil {
		return fieldsMap, fmt.Errorf("error uploading track: %v", err)
	}

	// Configure S3 bucket and key
	bucketName := track.env.AWS.BucketName

	//To get S3 key based on the file extension, partnerId, memberId and trackId
	key := getS3Key(fileExtension, partnerID, memberID, trackID)

	// To generates the content type for the file.
	contentType := generateContentType(fileHeader.Filename)

	//Upload the file to S3
	if err := track.UploadToS3AndHandleError(ctx, memberID, trackID, fileExtension, bucketName, key, contentType, fileHeader); err != nil {
		return fieldsMap, err
	}

	if err != nil {
		return fieldsMap, fmt.Errorf("error updating track status: %v", err)
	}

	return fieldsMap, nil
}

// decodeAudio decodes the audio using the provided audio.Decoder.
//
// Parameters:
//
//	@decoder: The audio.Decoder to decode the audio.
//
// Behavior:
//   - Decodes the audio using the given decoder.
//   - If decoding is successful, it returns the decoded audio.
//   - If there are any errors during decoding, it returns an error.
//
// Returns:
//   - The decoded audio if successful.
//   - An error if there are issues with decoding.
//
// Example:
//   - decodedAudio, err := decodeAudio(decoder)
//   - if err != nil {
//     // Handle the error
//     }
//   - // Use the decoded audio.
func decodeAudio(decoder audio.Decoder) (audio.Decoder, error) {
	decoder, err := decoder.Decode()

	if err != nil {
		return nil, fmt.Errorf("error decoding audio: %v", err)
	}
	return decoder, nil
}

// UploadTrack uploads an audio track to the system, along with its metadata.
//
// Parameters:
//
//	@ctx: The context for the upload operation.
//	@userID: The ID of the user performing the upload.
//	@PartnerID: The ID of the partner associated with the track.
//	@file: The multipart.File containing the audio data to upload.
//	@fileHeader: The multipart.FileHeader containing file information.
//
// Behavior:
//   - Validates user ID and file format.
//   - Extracts file extension, file name without extension, and file size in megabytes.
//   - Creates an audio decoder based on the file extension and decodes the audio.
//   - Extracts duration and sample rate from the decoded audio.
//   - Generates an ISRC code.
//   - Validates the audio duration and sample rate.
//   - Creates track metadata with the extracted information.
//   - Inserts track metadata into the system.
//   - Configures S3 bucket and key for storing the audio data.
//   - Uploads the audio data to an S3 bucket.
//   - Handles any errors during the upload process.
//
// Returns:
//   - A map of validation errors if any exist.
//   - An error if there are issues with the upload or data.
//   - Nil validation errors and nil error if the track is successfully uploaded.
//
// Example:
//   - fieldsMap, err := UploadTrack(ctx, userID, partnerID, file, fileHeader)
//   - if err != nil {
//     // Handle the error
//     }
//   - if len(fieldsMap) > 0 {
//     // Handle the validation errors
//     }
//   - // Track is successfully uploaded.
func (track *TrackUseCases) UploadTrack(ctx context.Context, userID string, PartnerID string, file multipart.File, fileHeader *multipart.FileHeader) (map[string][]string, error) {
	// Initialize fields map to hold validation errors
	fieldsMap := map[string][]string{}

	// Check if userID is provided
	if strings.TrimSpace(userID) == "" {
		utils.AppendValuesToMap(fieldsMap, "member_id", "required")
		return fieldsMap, fmt.Errorf("user ID is required")
	}

	fileExtension, fileNameWithoutExt, fileSizeInMb, err := ExtractFileInfo(fileHeader)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "unsupported_file")
		return fieldsMap, fmt.Errorf("unsupported file format")
	}

	// Check the file size
	if err = checkFileSize(fileSizeInMb, fieldsMap); err != nil {
		return fieldsMap, err
	}

	//To creates an audio decoder based on the file extension.
	decoder, err := createAudioDecoder(file, fileExtension)
	if err != nil {
		utils.AppendValuesToMap(fieldsMap, "common_message", "unsupported_file")
		return fieldsMap, fmt.Errorf("unsupported file format")
	}

	// Decode the audio file
	decoder, err = decoder.Decode()
	if err != nil {
		return fieldsMap, fmt.Errorf("error decoding audio: %v", err)
	}

	// To extracts duration and sample rate from the audio decoder.
	duration, samplerate, err := getAudioProperties(decoder)
	if err != nil {
		return fieldsMap, fmt.Errorf("error getting audio properties: %v", err)
	}

	//Get ISRC code

	isrc, err := track.repo.GenerateISRC(ctx, PartnerID)
	if err != nil {
		return fieldsMap, fmt.Errorf("error getting ISRC code: %v", err)
	}

	if err = checkDurationAndSampleRate(duration, samplerate, fieldsMap); err != nil {
		return fieldsMap, err
	}

	// Create track metadata
	trackMetadata := entities.TrackMetadata{
		ID:         uuid.New().String(),
		Name:       fileNameWithoutExt,
		FileName:   fileNameWithoutExt,
		Duration:   duration,
		CreatedOn:  time.Now(),
		MemberID:   userID,
		Status:     1,
		FileSize:   megabytesToBytes(fileSizeInMb),
		FileExt:    fileExtension[1:],
		SampleRate: int(samplerate),
		ISRC:       capitalizeLetters(isrc),
	}

	// Insert track metadata
	trackID, err := track.repo.InsertTrackMetadata(ctx, trackMetadata)
	if err != nil {
		return fieldsMap, fmt.Errorf("error uploading track: %v", err)
	}

	// Configure S3 bucket and key
	bucketName := track.env.AWS.BucketName

	//To get S3 key based on the file extension, partnerID, memberID and trackID
	key := getS3Key(fileExtension, PartnerID, userID, trackID)

	// To generates the content type for the file.
	contentType := generateContentType(fileHeader.Filename)

	if err := track.UploadToS3AndHandleError(ctx, userID, trackID, fileExtension, bucketName, key, contentType, fileHeader); err != nil {
		return fieldsMap, err
	}

	if err != nil {
		return fieldsMap, fmt.Errorf("error updating track status: %v", err)
	}

	customMessages := entities.QueueMessage{
		EventID:       uuid.New().String(),
		TrackID:       trackID,
		MemberID:      userID,
		PartnerID:     PartnerID,
		FileExtension: fileExtension,
	}

	contentBody := entities.MessageBody{
		TaskType: "convertion",
		Payload: map[string]interface{}{
			"inputPayload": customMessages,
		},
	}

	// Encode the custom message as JSON
	messageBody, err := json.Marshal(contentBody)
	if err != nil {
		log.Fatalf("Error encoding custom message: %v", err)
		return fieldsMap, fmt.Errorf("error encoding custom message: %v", err)
	}

	message, err := track.queue.ComposeMessage(context.TODO(), messageBody)
	if err != nil {
		log.Fatalf("Error composing message: %v", err)
		return fieldsMap, fmt.Errorf("error building message: %v", err)
	}

	if message != nil {
		if err := track.queue.Send(context.TODO(), message); err != nil {
			log.Fatalf("Error sending a message to the queue: %v", err)
			return fieldsMap, fmt.Errorf("error sending message: %v", err)
		}
	}

	return fieldsMap, nil
}

// megabytesToBytes converts megabytes to bytes.
func megabytesToBytes(megabytes float64) int64 {
	bytes := int64(megabytes * 1024 * 1024)
	return bytes
}

// capitalizeLetters converts lowercase letters to uppercase in a given string.
func capitalizeLetters(input string) string {
	var result string

	for _, char := range input {
		if char >= 'a' && char <= 'z' {
			// Convert lowercase letters to uppercase
			result += string(char - 32)
		} else {
			// Keep other characters as they are
			result += string(char)
		}
	}

	return result
}

// ExtractFileInfo extracts file information such as file extension, file name without extension,
// and file size in megabytes from a given multipart.FileHeader.
func ExtractFileInfo(fileHeader *multipart.FileHeader) (fileExtension string, fileNameWithoutExt string, fileSizeInMb float64, err error) {

	// Generate a unique filename based on member ID and timestamp
	fileExtension = filepath.Ext(fileHeader.Filename)
	fileNameWithoutExt = strings.TrimSuffix(fileHeader.Filename, fileExtension)

	// Get the file size in megabytes
	fileSizeInBytes := fileHeader.Size
	fileSizeInMb = float64(fileSizeInBytes) / (1024 * 1024)

	return fileExtension, fileNameWithoutExt, fileSizeInMb, nil
}

// checkFileSize checks if the file size is within valid limits.
func checkFileSize(fileSizeInMb float64, fieldsMap map[string][]string) error {

	if fileSizeInMb < consts.MinFileSizeMb || fileSizeInMb > consts.MaxFileSizeMb {
		utils.AppendValuesToMap(fieldsMap, "file_size", "format")
		return fmt.Errorf("invalid file size")
	}
	return nil
}

// CreateAudioDecoder creates an audio decoder based on the file extension.
func createAudioDecoder(file multipart.File, fileExtension string) (audio.Decoder, error) {

	// Initialize audio decoder configuration
	config := audio.DecoderConfig{
		DecimalLimit: "2",
	}

	// Create the appropriate audio decoder based on file extension
	var decoder audio.Decoder
	switch strings.ToLower(fileExtension) {
	case ".flac":
		decoder = audio.NewFlacDecoder(config).WithBytes(file)
	case ".aiff", ".aif":
		decoder = audio.NewAIFFDecoder(config).WithBytes(file)
	case ".wav":
		decoder = audio.NewWavDecoder(config).WithBytes(file)
	case ".mp3":
		decoder = audio.NewMP3Decoder(config).WithBytes(file)
	case ".ogg":
		decoder = audio.NewOGGDecoder(config).WithBytes(file)
	default:
		return nil, fmt.Errorf("unsupported file format: %s", fileExtension)
	}
	return decoder, nil
}

// GetAudioProperties extracts duration and sample rate from the audio decoder.
func getAudioProperties(decoder audio.Decoder) (float64, float64, error) {

	// Get audio duration and sample rate
	duration, err := decoder.Duration()
	if err != nil {
		return 0, 0, fmt.Errorf("error getting file duration: %v", err)
	}

	sampleRate, err := decoder.SampleRate()
	if err != nil {
		return 0, 0, fmt.Errorf("error getting file sample rate: %v", err)
	}
	return duration, sampleRate, nil
}

// CheckDurationAndSampleRate checks the duration and sample rate
func checkDurationAndSampleRate(duration float64, sampleRate float64, fieldsMap map[string][]string) error {

	if duration < consts.MinimumDuration {
		utils.AppendValuesToMap(fieldsMap, "duration", "format")
		return fmt.Errorf("invalid duration")
	}
	if sampleRate < consts.MinSamplingRateHz {
		utils.AppendValuesToMap(fieldsMap, "sample_rate", "format")
		return fmt.Errorf("invalid sample rate")
	}
	return nil
}

// GetS3Key returns the S3 key based on the file extension , partnerID, memberID and trackID
func getS3Key(fileExtension, partnerID, memberID, trackID string) string {

	if fileExtension == ".flac" {
		return fmt.Sprintf("tuneverse/%s/%s/tracks/%s", partnerID, memberID, trackID)
	}
	return fmt.Sprintf("tuneverse/temp/tracks/%s", trackID)
}

// GenerateContentType generates the content type for the file.
func generateContentType(fileName string) string {

	contentType := mime.TypeByExtension(filepath.Ext(fileName))
	if contentType == "" {
		contentType = consts.DefaultContentType
	}
	return contentType
}

// UploadToS3AndHandleError uploads the file to an S3 bucket and handles any errors.
func (track *TrackUseCases) UploadToS3AndHandleError(ctx context.Context, userID, trackID, fileExtension, bucketName, key, contentType string, fileHeader *multipart.FileHeader) error {

	err := track.cloud.UploadToS3(bucketName, key, fileHeader, contentType)
	if err != nil {
		status := 5
		if updateErr := track.repo.UpdateTrackStatus(ctx, userID, trackID, status); updateErr != nil {
			return fmt.Errorf("error updating track status: %v", updateErr)
		}
		return fmt.Errorf("error uploading to S3: %v", err)
	}
	status := 2
	if fileExtension == ".flac" {
		status = 4
	}
	if updateErr := track.repo.UpdateTrackStatus(ctx, userID, trackID, status); updateErr != nil {
		return fmt.Errorf("error updating track status: %v", updateErr)
	}
	return nil
}

// GetTrack retrieves information about a track, including its URL, by validating the track owner and fetching the track data.
//
// Parameters:
//
//	@ctx: The context for the operation.
//	@trackInfo: The Track struct containing track information, including its ID and the member's ID.
//
// Behavior:
//   - Validates if the track owner is valid based on the member's ID.
//   - Retrieves track data, such as metadata and status, using the track ID.
//   - Determines the S3 bucket key for fetching the track URL based on its status.
//   - Fetches the track URL from the S3 bucket.
//
// Returns:
//   - A map of validation errors if any exist.
//   - The track data, including its URL.
//   - An error if there are issues with validation, data retrieval, or S3 operations.
//
// Example:
//   - fieldsMap, trackData, err := GetTrack(ctx, trackInfo)
//   - if err != nil {
//     // Handle the error
//     }
//   - if len(fieldsMap) > 0 {
//     // Handle the validation errors
//     }
//   - // Use the track data, including the URL.
func (track *TrackUseCases) GetTrack(ctx context.Context, trackInfo entities.Track) (map[string][]string, entities.Track, error) {

	log := logger.Log().WithContext(ctx)
	fieldsMap := map[string][]string{}

	// Validate if the track owner is valid
	isValid, err := track.repo.ValidateTrackOwner(ctx, trackInfo.ID, trackInfo.MemberID)

	if err != nil {
		log.Errorf("ValidateTrackOwner Failed, %s", err.Error())
		return nil, entities.Track{}, err
	}

	if !isValid {
		log.Errorf("Validation failed, ValidateTrackOwner failed")
		utils.AppendValuesToMap(fieldsMap, "common_message", "invalid_track_member")
		return fieldsMap, entities.Track{}, nil
	}

	// Retrieve track data
	trackData, err := track.repo.GetTrack(ctx, trackInfo.ID)
	if err != nil {
		log.Errorf("GetTrack failed, err=%s", err.Error())
		return fieldsMap, entities.Track{}, err
	}

	var key string

	switch trackData.Status {
	case utilities.StatusToString(int(consts.Completed)), utilities.StatusToString(int(consts.Queued)):
		key = fmt.Sprintf("temp/tracks/%s", trackInfo.ID)
	default:
		key = fmt.Sprintf("%s/%s/tracks/%s", trackInfo.PartnerID, trackInfo.MemberID, trackInfo.ID)
	}

	bucketName := track.env.AWS.BucketName

	// Retrieve the track URL
	url, err := track.cloud.GetObject(ctx, bucketName, key)
	if err != nil {
		log.Errorf("GetTrack Failed, err%s", err.Error())
		return nil, entities.Track{}, err
	}

	trackData.URL = url
	return fieldsMap, trackData, nil
}

// UpdateConvertionStatus updates the status of a track.
func (track *TrackUseCases) UpdateConvertionStatus(ctx context.Context, userID string, trackID string, reqBody entities.UpdateTrackStatusRequest) error {
	// Call the repository method to update the track status
	err := track.repo.UpdateConvertionStatus(ctx, userID, trackID, reqBody)
	if err != nil {
		// Handle the error, for example, log it and return a specific error type
		return fmt.Errorf("failed to update track status: %v", err)
	}
	// If everything is successful, return nil
	return nil
}

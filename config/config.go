package config

import (
	"fmt"
	"os"
	"strings"
	"track/internal/entities"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/spf13/viper"
)

type Config struct {
	*viper.Viper
}

type ConfigType string

const (
	ConfigTypeYAML ConfigType = "yaml"
	ConfigFilePath            = "config"
	ConfigFileName            = "config.yml"
)

// LoadConfig loads the configuration for the application based on the given appName.
// It uses environment variables and yaml file configuration and the "envconfig" package to populate the configuration struct.
// The environment variables override the yaml file configuration if the same configurations are present.
func LoadConfig(appName string) (*entities.EnvConfig, error) {

	// Create an instance of the Configuration struct to hold the configuration data.
	configData := &Configuration{}

	// Load configuration from a YAML file named "config.yml" using the LoadServConfig function.
	err := LoadServConfig(ConfigFileName, ConfigFilePath, configData)
	if err != nil {
		return nil, err
	}

	// Check if a file named ".env" exists.
	// If it does, load environment variables from the file using the godotenv package.
	if _, err := os.Stat(".env"); err == nil {
		println("[ENV] Load env variables from .env")
		err := godotenv.Load()
		if err != nil {
			return nil, fmt.Errorf("error loading .env file: %w", err)
		}
	}

	// Populate the configuration struct with environment variables using the envconfig package.
	err = envconfig.Process(appName, &configData.EnvConfig)
	if err != nil {
		return nil, err
	}

	// Return the populated configuration struct.
	return &configData.EnvConfig, nil
}

// ValidateConfigPath checks if the specified path points to a valid file.
// It returns an error if the path does not exist or if it refers to a directory.
func ValidateConfigPath(path string) error {
	s, err := os.Stat(path)
	if err != nil {
		return err
	}
	if s.IsDir() {
		return fmt.Errorf("'%s' is a directory, not a normal file", path)
	}
	return nil
}

// LoadConfigFile loads a configuration file of a specified type (JSON, YAML, etc.)
// from the given path and file name using the Viper configuration library.
// It validates the path using ValidateConfigPath and returns a Config struct
// that wraps the Viper instance if successful, or an error if any issues occur.
func LoadConfigFile(configFile, configPath string, configType ConfigType) (*Config, error) {
	// Validate the configuration file path to ensure it is a valid file.
	if err := ValidateConfigPath(configPath + "/" + configFile); err != nil {
		return nil, err
	}

	// Create a new Viper instance for configuration management.
	v := viper.New()

	// Set the configuration file name, type, path, and configure environment variable bindings.
	v.SetConfigName(configFile)
	v.SetConfigType(string(configType))
	v.AddConfigPath(configPath)
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	// Find and read the configuration file using Viper.
	if err := v.ReadInConfig(); err != nil { // Handle errors reading the config file
		return nil, err
	}

	// Return a Config struct containing the Viper instance.
	return &Config{v}, nil
}

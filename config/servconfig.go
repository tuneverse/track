package config

import (
	"track/internal/entities"
)

var cfg *Configuration

// Configuration is a struct that holds the application's configuration data.
// It includes an EnvConfig field to store environment-specific configurations
// and a conf field that holds the Viper configuration instance.
type Configuration struct {
	EnvConfig entities.EnvConfig // Environment-specific configurations
	conf      *Config            // Viper configuration instance
}

// LoadServConfig loads server-specific configurations from a configuration file
// using the LoadConfigFile function and populates the Configuration struct.
// It returns an error if the configuration file loading or population fails.
func LoadServConfig(configFileName, configPath string, cfg *Configuration) error {
	config, err := LoadConfigFile(configFileName, configPath, ConfigTypeYAML)
	if err != nil {
		return err
	}

	cfgErr := cfg.populateServConfigs(config)
	if cfgErr != nil {
		return cfgErr
	}
	return nil
}

// populateServConfigs populates the server-specific configurations in the Configuration struct
// based on the provided Config instance.
func (ac *Configuration) populateServConfigs(config *Config) error {
	ac.conf = config

	// Populate audio file configurations from the configuration file.
	ac.EnvConfig.AudioFile.AcceptedSamplingRate = ac.conf.GetInt("audiofile.max_sampling_rate_in_hertz")
	ac.EnvConfig.AudioFile.AcceptedFileFormats = ac.conf.GetStringSlice("audiofile.supported_formats")
	ac.EnvConfig.AudioFile.Size.Maximum = ac.conf.GetFloat64("audiofile.size.maximum_in_kb")
	ac.EnvConfig.AudioFile.Size.Minimum = ac.conf.GetFloat64("audiofile.size.minimum_in_kb")
	ac.EnvConfig.AudioFile.Duration.Maximum = ac.conf.GetFloat64("audiofile.duration.maximum_in_sec")
	ac.EnvConfig.AudioFile.Duration.Minimum = ac.conf.GetFloat64("audiofile.duration.minimum_in_sec")

	return nil
}

// GetServConfig returns the server-specific configurations stored in the Configuration struct.
func GetServConfig() *entities.EnvConfig {
	return &cfg.EnvConfig
}

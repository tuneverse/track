package utilities

import (
	"fmt"
	"regexp"
	"strings"
	"track/internal/consts"

	"github.com/google/uuid"
)

// ValidateStringLength checks if the string length is between minLength && maxLength
func ValidateStringLength(value string, minLength, maxLength int) error {
	n := len(strings.TrimSpace(value))
	if n < minLength || n > maxLength {
		return fmt.Errorf("must contain from %d-%d characters", minLength, maxLength)
	}
	return nil
}

// ValidateISRC checks if an ISRC code is valid.
func ValidateISRC(isrc string) bool {
	// The pattern ensures that the ISRC code matches the format: CCRRRYYNNNNN
	// where C is a 2-letter country code, R is a 3-character registrant code,
	// Y is a 2-digit year code, and N is a 5-digit designation code.
	pattern := regexp.MustCompile(`^[A-Z]{2}[A-Z0-9]{3}\d{7}$`)

	// If the ISRC matches the pattern, it is considered valid.
	return pattern.MatchString(isrc)
}

// ValidateISWC checks if an ISWC code is valid.
func ValidateISWC(iswc string) bool {

	// The pattern ensures that the ISWC code matches the format: T-123456789-4
	pattern := regexp.MustCompile(`^[T]{1}[0-9]{9}\d{1}$`)

	// If the ISWC matches the pattern, it is considered valid.
	return pattern.MatchString(iswc)
}

// ConvertSliceToUUIDs converts a slice of interface{} to a slice of uuid.UUID.
func ConvertSliceToUUIDs(arg []interface{}) ([]uuid.UUID, error) {
	var result []uuid.UUID
	for _, item := range arg {
		id, ok := item.(string)
		if !ok {
			return nil, consts.ErrInvalidUUID
		}
		uuidValue, err := uuid.Parse(id)
		if err != nil {
			return nil, err
		}
		result = append(result, uuidValue)

	}
	return result, nil
}

// StatusToString converts an integer status code to its corresponding string representation.
func StatusToString(stat int) string {
	return [...]string{"started", "completed", "queued", "success", "failed"}[stat-1]
}

# Track Service

The Track Service module manages operations related to tracks, including uploading, updating, retrieving metadata, managing statuses, verifying genres, fetching member-specific data, and viewing lyrics. It provides a set of endpoints to interact with these functionalities.

## Endpoint Description

### Health Check

- Endpoint: GET /health
- Description: Checks the health status of the Track Service.
- Handler: HealthHandler

### Delete Track

- Endpoint: DELETE /:version/tracks/:track_id
- Description: Deletes a track by its ID.
- Handler: DeleteTrack

### Update Track Metadata

- Endpoint: PATCH /:version/tracks/:track_id
- Description: Updates metadata of a specific track.
- Handler: UpdateTrackMetadata

### Get Track

- Endpoint: GET /:version/tracks/:track_id
- Description: Retrieves detailed information about a specific track.
- Handler: GetTrack

### View All Tracks

- Endpoint: GET /:version/tracks
- Description: Retrieves a list of all tracks with metadata.
- Handler: ViewAllTracks

### Upload Track

- Endpoint: POST /:version/tracks
- Description: Uploads a new track.
- Handler: UploadTrack

### Update Track

- Endpoint: PUT /:version/tracks/:track_id
- Description: Updates details of an existing track.
- Handler: UpdateTrack

### Update Conversion Status

- Endpoint: PATCH /:version/tracks/:track_id/update-status
- Description: Updates the conversion status of a track.
- Handler: UpdateConvertionStatus

### Track Genre Verification

- Endpoint: HEAD /:version/tracks/genres/:id
- Description: Verifies the genre of a track by ID.
- Handler: TrackGenerVerify

## Get Track Member Count

- Endpoint: GET /:version/tracks/member/:member_id
- Description: Retrieves the count of tracks associated with a specific member.
- Handler: GetTrackMemberCount

### Add Track Lyrics

- Endpoint: PATCH /:version/tracks/:track_id/lyrics
- Description: Updates the lyrics of a specific track.
- Handler: AddLyrics

### View Track Lyrics

- Endpoint: GET /:version/tracks/:track_id/lyrics
- Description: Retrieves the lyrics of a specific track.
- Handler: ViewLyrics

### Delete Track Lyrics

- Endpoint: DELETE /:version/tracks/:track_id/lyrics
- Description: Deletes the lyrics of a specific track.
- Handler: DeleteLyrics

## Environment Variables

- TRACK_DEBUG: Indicates whether debugging is enabled for the Track Service. (Example: true)
- TRACK_DB_PORT: The port number on which the Track Service database is running. (Example: 5433)
- TRACK_DB_USER: The username for connecting to the Track Service database. (Example: postgres)
- TRACK_DB_PASSWORD: The password for the database user to connect to the Track Service database. (Example: tuneverse)
- TRACK_DB_DATABASE: The name of the Track Service database. (Example: tuneverse_local)
- TRACK_ACCEPTED_VERSIONS: The accepted versions of the Track Service. (Example: v1_0)
- TRACK_DB_SCHEMA: The schema within the Track Service database. (Example: public)
- TRACK_DB_HOST: The hostname or IP address of the Track Service database server. (Example: 10.1.0.229)
- TRACK_PORT: The port on which the Track Service is running. (Example: 8020)
- TRACK_LOCALISATION_SERVICE_URL: The URL for the localization service used by the Track Service. (Example: http://10.1.0.206:8025/api/v1)
- TRACK_CACHE_EXPIRATION: The expiration time (in hours) for the Track Service cache.
- TRACK_ENDPOINT_URL: The URL for the main endpoint of the Track Service. (Example: http://10.1.0.206:8025/api/v1)
- TRACK_ERROR_HELP_LINK: A URL linking to a help document for errors related to the Track Service. (Example: https://docs.google.com/spreadsheets/d/1dgBRdaj-xVt5DcrBNIEjqjzH5paAmKyKJ4DeihXvcDo/edit?usp=sharing)
- TRACK_LOGGER_SERVICE_URL: The URL for the logger service used by the Track Service. (Example: http://10.1.0.90:8040/api/v1.0)
- TRACK_LOGGER_SECRET: The secret key for authenticating with the logger service. (Example: "logger secret")
- TRACK_AWS_ACCESS_KEY: The AWS access key for accessing AWS services. (Example: AKIAVWRJX4ZI2O4567TY)
- TRACK_AWS_ACCESS_SECRET: The AWS secret key for accessing AWS services. (Example: A//obTDIL8FFj3MEmr3lDnl12ByDzu/6/UsIDUDf)
- TRACK_AWS_REGION: The AWS region where services are deployed. (Example: us-east-1)
- TRACK_AWS_BUCKET_NAME: The name of the AWS S3 bucket used by the Track Service. (Example: tuneverse-bucket)
- TRACK_AWS_SQS_QUEUE_NAME: The name of the AWS SQS queue used by the Track Service. (Example: tuneverse)
- TRACK_AWS_SQS_QUEUE_URL: The URL of the AWS SQS queue used by the Track Service. (Example: https://sqs.us-east-1.amazonaws.com/392003315281/tuneverse)
- TRACK_ISRC_SERIES: The ISRC series identifier used by the Track Service. (Example: inxyz)
- TRACK_MESSAGE_QUEUE_TYPE: The type of message queue used by the Track Service. (Example: sqs)
- TRACK_RABBIT_MQ_CONFIG_USER: The RabbitMQ username for configuration. (Example: tuneverse)
- TRACK_RABBIT_MQ_CONFIG_PASSWORD: The RabbitMQ password for configuration. (Example: tuneverse@123)
- TRACK_RABBIT_MQ_CONFIG_HOST: The hostname or IP address of the RabbitMQ server used for configuration. (Example: 10.1.0.226)
- TRACK_RABBIT_MQ_CONFIG_PORT: The port number of the RabbitMQ server used for configuration. (Example: 5673)
- TRACK_RABBIT_MQ_CONFIG_QUEUE: The name of the RabbitMQ queue used for configuration. (Example: hari_test)
- TRACK_RABBIT_MQ_CONFIG_DURABLE: Indicates if the RabbitMQ queue used for configuration is durable. (Example: true)
- TRACK_SUPPORTED_EXTENSIONS: The supported file extensions for tracks managed by the Track Service. (Example: ".flac,.aiff,.aif,.wav,.mp3,.ogg,.m4a")
- TRACK_MEMBER_SERVICE_URL: The URL for the member service used by the Track Service. (Example: http://10.1.0.137:8039/api/v1.0/members)
- TRACK_PARTNER_SERVICE_URL: The URL for the partner service used by the Track Service. (Example: http://10.1.0.90:8031/api/v1/partners)
- TRACK_ACTIVITY_LOG_SERVICE_URL: The URL for the activity log service used by the Track Service. (Example: http://10.1.0.206:8026/api/v1.0)
- TRACK_ACCESS_CONTROL_SERVICE_URL: The URL for the access control service used by the Track Service. (Example: http://10.1.0.229:8080/api/v1.0/access-control)
- TRACK_UTILITY_SERVICE_URL: The URL for the utility service used by the Track Service. (Example: http://10.1.0.111:8080/api/v1.0)
- TRACK_PRODUCT_SERVICE_URL: The URL for the product service used by the Track Service. (Example: http://10.1.0.206:8080/api/v1.0)
- TRACK_ARTIST_SERVICE_URL: The URL for the artist service used by the Track Service. (Example: http://10.1.0.137:8032/api/v1.0)
